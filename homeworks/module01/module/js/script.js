/* Initial song list */
let songs = [{
  name: 'Jingle Bells',
  isLiked: false,
}, {
  name: 'We Wish You a Merry Christmas',
  isLiked: true,
}];

//создаю логику через основную функцию
function addSong(array) {
  for (let arrItem of array) {
    let main = document.querySelector('.playlist-sect');
    let songItem = document.createElement('div');
    songItem.setAttribute('class', 'song-item');
    let img = document.createElement('img');
    img.setAttribute('src', 'images/like.svg');
    let buttonLike = document.createElement('button');
    buttonLike.textContent = 'Unlike';
    buttonLike.setAttribute('class', 'like-button');
    let paragraph = document.createElement('p');
    paragraph.textContent = arrItem.name;
    let buttonRemove = document.createElement('button');
    buttonRemove.textContent = 'Remove';
    buttonRemove.setAttribute('class', 'remove-button');
    main.append(songItem);
    songItem.append(img);
    songItem.append(buttonLike);
    songItem.append(paragraph);
    songItem.append(buttonRemove);

    if (!arrItem.isLiked) {
      buttonLike.textContent = 'Like';
      img.setAttribute('class', 'unlike');
    }
  }
}
//прогружаю первичные данные
window.addEventListener('load', function () {
  addSong(songs);
});
// добавить новую песню
const addButton = document.querySelector('.button-add');
addButton.addEventListener('click', function () {
  const inputBox = document.querySelector('.input-box');
  let newSong = [{
    name: inputBox.value,
    isLiked: false
  }];
  addSong(newSong);
  inputBox.value = '';
  songCounter.textContent = `${document.body.children[3].children.length}`;
});
// подозреваю что логика записанная ниже имеют более короткую форму записи
//определяю что будут делать клики на кнопки лайк/анлайк и ремув
document.addEventListener('click', function (event) {
  switch (event.target.className) {
    case 'like-button':
      switch (event.target.outerText) {
        case 'Like':
          event.target.textContent = 'Unlike';
          event.target.previousSibling.style.filter = 'grayscale(0)';
          songCounter.textContent = `${document.body.children[3].children.length}`;
          break;
        case 'Unlike':
          event.target.textContent = 'Like';
          event.target.previousSibling.style.filter = 'grayscale(1)';
          songCounter.textContent = `${document.body.children[3].children.length}`;
          break;
      }
      break;
    case 'remove-button':
      event.target.parentElement.remove();
      songCounter.textContent = `${document.body.children[3].children.length}`;
      break;
  }
});
// создаю переменную для подсчета песен в плейлисте, задаю ей значение начального массива песен и переопределяю в функциях, которые меняют кол-во
let songCounter = document.querySelector('.count');
songCounter.textContent = `${songs.length}`;

