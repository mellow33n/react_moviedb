//1) Напишіть функцію fillArray, яка створює масив і заповнює його заданим значенням.
function fillArray (number, value) {
    let arr = Array(number).fill(value);
    return arr;
}
{
let array = fillArray(3, 'qwerty');
console.log(array); // ['qwerty', 'qwerty', 'qwerty']
}
// 2) Напишіть функцію reverseArray, яка перевертає значення масиву задом наперед.
function reverseArray (arr) {
    return arr.reverse();
}
{
let array = ['My', 'life', '-', 'my', 'rules'];
let result = reverseArray(array);
console.log(result); 
}

// 3) Напишіть функцію filterArray, яка очищує масив від небажаних значень (false, undefined, '', 0, null, NaN).
function filterArray (arr) {
    arr = arr.filter(Boolean);
    return arr; 
} 
{
let array = [0, 1, 2, null, undefined, 'qwerty', false, NaN];
let result = filterArray(array);
console.log(result); // [1,2, 'qwerty'];
}
// 4) Напишіть функцію spliceFour, яка видаляє 4й елемент масиву і замінює його строкою 'JavaScript'
function spliceFour (arr) {
    removedFromArr = arr.splice(3, 1, 'JavaScript');
    return arr;
}
{
let array = [1, 2, 3, 4, 5];
let result = spliceFour(array);
console.log(result); // [1, 2, 3, 'JavaScript', 5];
}
//5) Напишіть функцію joinArray, яка перетворює масив в строку, з'єднуючи елементи заданим символом.
function joinArray(arr, symbol) {
    return arr.join(symbol);
}
{
let array = [1, 2, 3, 4, 5];
let result = joinArray(array, '%');
console.log(result); // 1%2%3%4%5
}

//6) Напишіть функцію joinStr, яка повертаэ строку, яка утворена з конкатинацїї усіх строк, які передані в якості аргументів функції через кому.
function joinStr (...arr) {
    arr = arr.filter(function(i) {
        return typeof i === 'string';
    });
    return arr.join();
}
{
let string1 = joinStr(0);
console.log(string1); // ''
let string2 = joinStr(1,'hello',3, 'world');
console.log(string2); // 'hello,world'
let string3 = joinStr('g','o', 0, '0', null, 'd', {});
console.log(string3); // 'g,o,0,d'
}
/* 7) Напишіть функцію advancedFillArray, яка створює масив і заповлює його випадковими значеннями чисел з заданого діапазону. (Логіку створення випадкового числа оберніть в функцію setRandomValue(min, max)). */

/* де 5 - це довжина масива, 1 - це мінімальне значення, 15 - це максимальне значення.
tips: тут вам станеться у нагоді функція fillArray, яку ви вже написали в першому завданні та метод масиву map для того щоб змінити значення на потрібні. */


function advancedFillArray (lenghtArr, min, max) {
    let array = fillArray(lenghtArr, 0);
    let arrayMap = array.map(() => randomInt(min, max));
    return arrayMap;
}
function randomInt(min, max) {
    let randomNum = min - 0.5 + Math.random() * (max - min + 1); 
    randomNum = Math.round(randomNum);
    return randomNum;
}

let array  = advancedFillArray(5, 1, 15);
console.log(array); 


