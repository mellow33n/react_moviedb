/* 1) Создайте функцию createBuffer(), которая создает локальную переменную text в виде пустой строки и возвращает функцию buffer, с которой можно работать следующим образом:

Если в функцию buffer был передан строковый параметр – этот параметр записывается в переменную text. Иначе выведите ошибку в консоль.
Если функция buffer вызывается без параметров – возвращается значение переменной text.
Создайте переменную и запишите в нее результат выполнения функции createBuffer().
Продемонстрируйте работу возвращаемой функции buffer с параметром и без.
 */
function createBuffer () {
    let text = '';
        function buffer(stringValue) {
            switch (true) {
                case typeof stringValue === 'string':
                    text += stringValue;
                    return text;
                case stringValue === undefined:
                    return text;
                default:
                    return console.error('error, type string value');
            }
        }
    return buffer;
}
let buffer = createBuffer();
buffer("A");
buffer("XYZ");
console.log(buffer()); // AXYZ

/* 2) Создайте функцию signUp(), которая принимаем параметры userName, password и возвращает объект со свойством userName и методом signIn().
Метод signIn() принимает параметр newPassword и сравнивает его значение со значением password.
Если пароли совпадают – метод возвращает сообщение 'Sign in success for ${userName}' , иначе – 'Password is incorrect' .
Создайте переменную user и запишите в нее результат вызова функции signUp().
Результат работы функции выведите в консоль. */

function signUp(userName, password) { 
    
    return { userName: userName, signIn: function signIn (newPassword) {
                if (password === newPassword) {
                    console.log(`Sign in success for ${userName}`);
                } else {
                    console.log(`Password is incorrect`);
                }
            }};
}
    
let user = signUp("billy", "qwerty");
user.signIn("a"); 
user.signIn("qw4"); 
user.signIn("asd");
user.signIn("qwerty");  
user = signUp("john", "asdfg");
user.signIn("asdfg");
user.signIn("qwerty");

