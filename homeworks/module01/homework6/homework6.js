/* 1) Напишіть програму, яка продемонструє роботу з масивом. Створіть масив із восьми елементів:
'455' 87.15 true undefined null 'false' [] {}
Виведіть інформацію о типі даних кожного елемента в консолі.  */
{

const arr = ['455', 87.15, true, undefined, null, 'false', [], {}];
for (let i = 0, arrLen = arr.length; i < arrLen; i++) {
    console.log(typeof arr[i]);
}
// Додайте значення 7 до кожного елементу масива і виведіть отримані значення в консолі.
for (i = 0, arrLen = arr.length; i < arrLen; i++) {
    arr[i] += 7;
}
console.log(arr);

}

/* 2) Напишіть програму, яка питає у користувача число і створює масив numbers з випадкових цілих чисел в діапазоні від 0 до 10, довжина якого дорівнює числу, яке ввів користувач.
Виведіть створений масив numbers в консолі.
 */


let number = +prompt('Enter your number > 0');

while (isNaN(number) || number <= 0) {
    console.error('invalid value');
    number = +prompt('Enter your number > 0');
}

let numbers = [];
for (let i = 0; ; i++) {
    if (i === number) {
        console.log(numbers);
        break;
    }
    numbers[i] = Math.floor(Math.random() * 10); 
}
/* Скопіюйте массив numbers в новий масив. Кожен третій елемент нового масиву помножте на 3.
Виведіть новостворений масив в консолі. */
let newArr = JSON.stringify(numbers);
newArr = JSON.parse(newArr);

for(i = 2; ; i += 3) {
    if (i > newArr.length - 1) {
        console.log(newArr);
        break;
    }
    newArr[i] *= 3;
}

/* 3) Працюю з файлом employee.js. Зверніть увагу на підключення масиву співробітників. Він знаходиться в окремому файлі (employee.js) і підключен вище основного js файлу (script.js), де буде розташована ваше дз. Таким чином масив empolyee буде доступний в файлі script.js, хоча по факту там знаходитись не буде.
 */
/* а) Створіть массив, який складається з повних імен всіх співробітників.
const fullNames = /* your code */ // ['Karan Duffy', 'Brax Dalton', 'Jody Lam', ...] */

const fullNames = [];
for (i = 0; ; i++) {
    if (i > empolyee.length - 1) {
        break;
    }
    fullNames[i] = `${empolyee[i]['name']} ${empolyee[i]['surname']}`;

}
// b) Знайдіть середнє значення всіх зарплат співробітників.
let sumSalary = 0;
for (i = 0; ; i++) {
    if (i > empolyee.length - 1) {
        break;
    }
    sumSalary += empolyee[i]['salary'];
}
// відповідь:
const avarage = (sumSalary / empolyee.length).toFixed(2);

// с) Виведіть в консоль імʼя чоловіка-пільговика (ключ isPrivileges=true) з самою великою зарплатою.
let candidate = '', candidateSalary = 0;
for (i = 0; ; i++) {
    if (i > empolyee.length - 1) {
        break;
    }
    if (empolyee[i]['gender'] === 'male' && empolyee[i]['salary'] > candidateSalary) {
        candidate = empolyee[i]['name'];
        candidateSalary = empolyee[i]['salary'];
    }
}
// відповідь:
const maxPrivilegesMan = candidate;
console.log(maxPrivilegesMan);

//d) Виведіть в консоль повні імена (імʼя + прізвище) двох жінок з самим маленьким досвідом роботи (ключ workExperience).

let lastExp = 0, preLastExp = 0, idDetectLast = 0, idDetectPreLast = 0, j = 0;
// знаходимо першу жінку в массиві 
while (empolyee[j]['gender'] === 'male') {
    j++;
}
// визначаю першу знайденну жінку як з самим маленьким досвідом
lastExp = empolyee[j]['workExperience'];
idDetectLast = j;
// продовжую перебирати массив
while (empolyee.length > j ) {
    // визначаю переможницю конкурса
    if(empolyee[j]['gender'] === 'female' && empolyee[j]['workExperience'] < lastExp) {
        preLastExp = lastExp;
        idDetectPreLast = idDetectLast;
        lastExp = empolyee[j]['workExperience'];
        idDetectLast = j;
    }
    // додаю умови щоб дівчатки могли змагатись за друге місце
    if (empolyee[j]['gender'] === 'female' && 
        empolyee[j]['workExperience'] > lastExp && 
        empolyee[j]['workExperience'] < preLastExp) {
        preLastExp = empolyee[j]['workExperience'];
        idDetectPreLast = j;
    }
    j++;
}

console.log(`${fullNames[idDetectLast]} , ${fullNames[idDetectPreLast]}`);


//e) Виведіть в консоль інформацію, скільки всього заробили співробітники за весь час роботи в одній строці. Формат відповіді: <імʼя прізвище> - <сума>.

const sumSalaryTotal = [];
for (i = 0; ; i++) {
    if (i > empolyee.length - 1) {
        break;
    }
    sumSalaryTotal[i] = (empolyee[i]['salary']) * (empolyee[i]['workExperience']);
}
for (i = 0; ; i++) {
    if (i > empolyee.length - 1) {
        break;
    }
    console.log(`${fullNames[i]} - ${sumSalaryTotal[i]}`);
}




