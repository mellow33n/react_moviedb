/* 1) Создайте объект coffeeMachine со свойством message: ‘You coffee is ready!’ и методом start(), при вызове которого через 3 секунды в консоль выведется сообщение, записанное в свойстве message.
Начальный код: */
let coffeeMachine = {
    message: 'Your coffee is ready!',
    start: function() {
     setTimeout(function(){
        return console.log(`${this.message}`); }.bind(this) , 3000);
    },
};

coffeeMachine.start(); // 'Your coffee is ready!'

//Создайте объект teaPlease со свойством message: 'Wanna some tea instead of coffee?'. Обновите методу start() контекст так, чтобы он выводил сообщение с нового объекта.
let teaPlease = {
    message: 'Wanna some tea instead of coffee?'
};
coffeeMachine.start.call(teaPlease); // 'Wanna some tea instead of coffee?'

/* 2) Напишите функцию concatStr(), которая соединяет две строки, разделенные каким-то символом: разделитель и строки передаются в параметрах функции. 
Используя привязку аргументов с помощью bind, создайте новую функцию hello(), которая которая выводит приветствие тому, кто передан в ее параметре: */
let concatStr = function(strOne, sep, strTwo) {
    return strOne + sep + strTwo;
};
let hello = concatStr.bind(null, 'Hello',' ');

let checkConcat = concatStr('Hello', ' ', 'Matt'); // 'Hello Matt'
let finalResult = hello('Matt'); // 'Hello Matt'
finalResult = hello('John'); // 'Hello John

/* 3) Напишите функцию showNumbers(), которая последовательно выводит в консоль числа в заданном диапазоне, с заданным интервалом (все данные должны передаваться как параметры функции).
tips: для реализации используйте функцию setInterval() */

function showNumbers(min, max, delay) {
    setTimeout(function(){
        for(; min <= max; min++) {
        console.log(min);
        }
    }, delay);
}
showNumbers(5, 10, 500); // 5 6 7 8 9 10

/* 4) Какой результат выполнения будет у данного кода? Объяснить почему. */
function addBase(base) {
    return function (num) {
      return base + num;
    };
  }
  let addOne = addBase(1);
  alert(addOne(5) + addOne(3));

 /*Ответ:   Браузер показал 10. Я попытался разобратся почему:
            Функция addBase принимает аргумент и возвращает результат анонимной функции, которая вызывается с незаданным аргументом num.
            Далее объявлется переменная, её значение задается как функция addBase с аргументом === 1. 
            Теперь, как мне кажется, при вызове функции addOne с аргументом - этот аргумент будет передан в незаданную переменную num функции addBase.
            Видимо это как-то связанно с тем, что обе функции ссылаются на одно место хранения.
             */


