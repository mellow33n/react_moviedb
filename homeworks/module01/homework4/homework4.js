//1) Напишіть програму, яка питає у користувача його імʼя і виводить в консолі текстове привітання:
/* //Happy birthday to you
Happy birthday to you
Happy birthday, dear <name>
Happy birthday to you */

let userName = prompt('Enter your name');
for (let i = 0; i <= 3 ; i++) {  
    if (i === 2) {
    console.log(`Happy birthday, dear ${userName}`);
    continue;
    }
    console.log('Happy birthday to you');
}
//2) Сформуйте строку '.#.#.#.#.#.#.#' за допомогою циклу for, де необхідну кількість повторів символів '.#' задає користувач через команду prompt().
let userIter = +prompt('type a number of iteration the symbols\(".#"\)');
let result = '';
for (let cycleIter = 1; userIter > 0; cycleIter++) {
    result += '.#';
    if (cycleIter === userIter) {
        break;
    }
}
console.log(result);

/* 3) Напишіть програму, яка питає у користувача число і сумує всі непарні числа до цього числа.
Якщо користувач ввів не число або відʼємне число чи 0, визивати команду prompt() з текстом "Invalid. You should enter a number" до тих пір поки вірний формат даних не буде введений користувачем.
Результат відобразіть в консолі. */

let userOddNumber = +prompt('Enter a number');
let sumOdd = 0;
while (userOddNumber <= 0 || isNaN(userOddNumber)) {
    console.log('Invalid. You should enter a number');
    userOddNumber = +prompt('Enter a number');
} 
if(userOddNumber > 0) {
    for (i = 1; i < userOddNumber; i += 2) {
        sumOdd += i;
    }
}
console.log(sumOdd);

//4) Напишіть нескінченний цикл, який закінчується командою break, коли Math.random() > 0.7. Виведіть в консоль число, на якому переривається цикл та відобразіть в консолі кількість ітерацій циклу.
/* Loop was finished because of: <number>
Number of attempts: <number></number> */
let randomNum = Math.random();
let attemptsNum = 0;
for (; randomNum < 0.7; attemptsNum++) {
    randomNum = Math.random();
    if (randomNum > 0.7) {
        break;
    }
}
console.log(`Loop was finished because of: ${randomNum}`);
console.log(`Number of attempts: ${attemptsNum}`);

/* 5) Напишіть цикл від 1 до 50, в якому будуть виводитися числа по черзі від 1 до 50, при цьому:
Якщо число ділиться на 3 без залишку, то виведіть не це число, а слово 'Fizz';
Якщо число ділиться на 5 без залишку, то виведіть не це число, а слово 'Buzz';
Якщо число ділиться і на 3 і на 5 одночасно, то виведіть не це число, а слово 'FizzBuzz'; */

let cycleNumber = 1;
while (cycleNumber <= 50) {
    switch (true) {
        case cycleNumber % 3 === 0 && cycleNumber % 5 === 0:
            console.log('FizzBuzz');
            break;
        case cycleNumber % 3 === 0:
            console.log('Fizz');
            break;
        case cycleNumber % 5 === 0:
            console.log('Buzz');
            break;
        default:
            console.log(cycleNumber);
            break;
    }
    cycleNumber++;
}
/* 6) Напишіть програму, яка знайде всі роки, коли 1 січня випадає на неділю у період між 2015 та 2050 роками включно (зверніть увагу, що 1 січня в лапках).
"1st of January" is being a Sunday in <year> */
let startDate = new Date('2015-01-01');
let endDate = new Date('2050-01-01');
while (startDate <= endDate) {
    if (startDate.getDay() === 0) {
        console.log(`"1st of January" is being a Sunday in ${startDate.getFullYear()}`);
    }
    startDate.setFullYear(startDate.getFullYear() + 1);
}
