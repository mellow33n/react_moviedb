// Homework 03

// 1) Создайте три переменные. Присвойте первой переменной числовое значение. Вторая переменная должна быть равна первой переменной, увеличенной в 3 раза. Третья переменная равна сумме двух первых. Выведите в консоль все три переменные.

let firstVar = 2;
let secondVar = firstVar * 3;
let thirdVar = firstVar * secondVar;
console.log(firstVar, secondVar, thirdVar);

//2) Создайте переменные х и y для хранения числа. Значения переменных задает пользователь через команду prompt(). Рассчитайте произведение, частное, разность и сумму этих значений. Результат последовательно отобразите в консоли в формате полной математической операции:

let numberX = parseFloat(prompt('type data number "x"'));
let numberY = parseFloat(prompt('type data number "y"'));
let sum = numberX + numberY;
let difference = numberX - numberY;
let multiplication = numberX * numberY;
let fraction = numberX / numberY;
console.log(`${numberX} + ${numberY} = ${sum.toFixed(3)}`);
console.log(`${numberX} - ${numberY} = ${difference.toFixed(3)}`);
console.log(`${numberX} * ${numberY} = ${multiplication.toFixed(3)}`);
console.log(`${numberX} / ${numberY} = ${fraction.toFixed(3)}`);

//3) Создайте переменную str и запишите в нее значение из prompt. Переведите ее в верхний регистр, посчитайте длину строки и выведите эти данные в консоль в форматe (это все должно быть записано в одном console.log()):

let str = prompt('type any information');
console.log(`You wrote: "${str}" \\ it's length ${str.length}. \nThis is your big string: "${str.toUpperCase()}". \n\tAnd this is a small one: "${str}".`);



// 4) В пачке бумаги 500 листов. За неделю в офисе расходуется 1200 листов. Какое наименьшее количество пачек бумаги нужно купить в офис на 8 недель? Результат отобразите в консоли (команда console.log()).
let paperBox = 500;
let weekPaperDemand = 1200;
let weekStock = 8;
let weekStockDemand = parseInt(weekPaperDemand * weekStock / paperBox) + 1;
console.log(`${weekStockDemand}`);

//5) Напишите программу которая считает процент от суммы по кредиту. Процент и сумму получаем от пользователя (команда prompt()), результат выводим в консоль (команда console.log()). Учитывайте, что пользователь может отправить процент как 10 или же как 10%.
let userCreditBody = parseFloat(prompt('type your amount of credit'));
let userCreditPercent = parseFloat(prompt('type your percent of credit'));
let userCreditDebt = userCreditBody * ((userCreditPercent / 100) + 1);
console.log(`If you repay the debt within the agreed period, the amount due will be ${userCreditDebt.toFixed(2)}`);




