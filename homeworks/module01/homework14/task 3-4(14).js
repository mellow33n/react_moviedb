/* 3) Запросите у пользователя число и добавьте в форму теги <input> (перед кнопкной Register) равных этому числую.

Требования к тегу <input>:

каждый инпут должен содержать класс input-item, value = `Input ${index}`;
последний инпут должен иметь дополнительный класс margin-zero;
создайте собственный класс, со свойством background-color и добавьте его всем нечетным инпутам;
очистите значение каждому третьему инпуту и задайте ему атрибут placeholder с любым текстом. */

const userNumber = +prompt('type any number');

function addInput(number) {
    const form = document.querySelector('form');
    let index = 0;
    while (index < number) {
        const input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('value', `Input ${number}`);
        input.classList.add('input-item');
        form.prepend(input);
        number--;
    }
    let lastInput = document.querySelector('.input-item:last-of-type');
    lastInput.classList.add('margin-zero');
    let oddInputs = document.querySelectorAll('.input-item:nth-of-type(2n+1)');
    for (let classInput of oddInputs) {
        classInput.classList.add('my-class');
        classInput.style.backgroundColor = 'lightblue';
    }
    let everyThirdInput = document.querySelectorAll('.input-item:nth-of-type(3n)');
    for (let attr of everyThirdInput) {
        attr.setAttribute('value', '');
        attr.setAttribute('placeholder', 'any text');
    }
    return form;
}
addInput(userNumber);

/* 4) (опционально) Напишите функцию, которая принимает аргумент number, создает матрицу в виде таблицы следующего типа:
первая ячейка в первой строке содержит значение «1», а каждая следующая ячейка должна быть больше на «1»;
первая ячейка второй строки содержит значение «2» и так далее...
Напишите фукнцию, которая меняет цвет ячейкам в таблице, расположенным на обратной
диагонали матрицы. */

function createMatrixTable(number) {
    const table = document.getElementById('matrix');
    let i = 0;
    let j = 0;
    //создаю пустую таблицу
    while (i < number) {
        const line = document.createElement('tr');
        table.prepend(line);
        while (j < number) {
            const item = document.createElement('th');
            item.setAttribute('class', 'table-item');
            line.prepend(item);
            j++;
        }
        i++;
        j = 0;
    }
    i = 0;
    // создаю правило заполнение ячеек 
    i = 1;
    j = 1;
    let countLines = 1;
    const tableItems = document.querySelectorAll('.table-item');
    for (let content of tableItems) {
        content.textContent = i;
        content.classList.add(`n${i}`); // заготовка на будущее
        i++;
        if (j === number) {
            countLines++;
            i = countLines;
            j = 0;
        }
        j++;
        //обратная диагональ матрицы, в данной таблице, всегда имеет значение аргумента функции. 
        let reverseDiagonal = document.querySelectorAll(`.n${number}`);
        for (let styles of reverseDiagonal) {
            styles.style.background = 'purple';
        }
    }
    return table;
}
createMatrixTable(12);