/* 1) Напишите функцию, которая изменяет фоновый цвет текста последнего параграфа в блоке
<body>, а так же функцию, которая меняет блоки footer и main местами. */

const lastP = document.querySelector(`p:last-of-type`);
let choosenDiv1 = document.getElementById('wrapper');

function setTagColor(tags, color) {
    tags.style.backgroundColor = color;
    return tags;
}

function reverseDivs(div) {
    div.style.display = 'flex';
    div.style.flexDirection = 'column-reverse';
    return div;
}

setTagColor(lastP, 'green');
reverseDivs(choosenDiv1);

/* 2) Напишите функцию, которая спрашивает у пользователя разрешения добавить картинку (confirm()) и в случае согласия добавляет картинку на страницу (ссылку на картинку пользователь должен задавать самостоятельно). */
let userSolution = confirm('Do you want add a picture ?');
let userLink = '';
if (userSolution) {
    userLink = prompt('Paste your image link');
}
let choosenDiv2 = document.getElementById('wrapper-task-2');

function setImage(div, link) {
    let appendImg = document.createElement('img');
    appendImg.setAttribute('src', link);
    div.append(appendImg);
    return div;
}
setImage(choosenDiv2, userLink);