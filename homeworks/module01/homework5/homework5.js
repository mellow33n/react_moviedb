/* 1) Напишіть програму, яка рахує, в якій чверті на годиннику знаходиться вказане користувачем число в хвилинах. Перевірте, щоб число було цілим і в проміжку між 0 і 59. Результат вивести в консоль.
Якщо number === 15, то виведе 2
Якщо number === 48, то виведе 4
Якщо number === 59, то виведе 4
Якщо number === 150, то виведе повідомлення про помилку */
{
let number = +prompt('Enter your number');

if (isNaN(number) || !Number.isInteger(number) || number < 0) {
  console.error('Enter not a integer number');
} else {
    switch (true) {
        case number <= 14:
            console.log(1);
            break;
        case number >= 15 && number <= 29:
            console.log(2);
            break;
        case number >= 29 && number <= 44:
            console.log(3);
            break;
        case number >= 44 && number <= 59:
            console.log(4);
            break;
        default:
            console.error('enter a valid number \[1-59\]');
            break;
    }
}
}
/* 2) Напишіть програму, яка питає у користувача число і виводить в консоль наступну інформацію про нього:
- додатнє воно чи від'ємне;
- скільки цифр в цьому числі;
- якщо число додатне, виведіть суму його цифр
Якщо number === 0, то виведе '0, lenght: 1'
Якщо number === 100500, то виведе 'positive, length: 6, sum: 6'
Если number === -50, то виведе 'negative, length: 2' */
{
let number = +prompt('Enter your number');


if (isNaN(number) || !Number.isInteger(number)) {
    console.error('Enter not a integer number');
} else {
    switch (true) {
        case number === 0:
            console.log(`${number}, length: ${(number + '').length} `);
            break;
        case number > 0:
            let sum = 0;
            let numberForLoop = number;
            while (numberForLoop > 0) {
                sum += numberForLoop % 10;
                numberForLoop = Math.floor(numberForLoop/10);
            }
            console.log(`positive, length: ${(number + '').length}, sum: ${sum} `);
            break;
        case number < 0:
            console.log(`negative, length: ${(number + '').length - 1} `);
            break;
    }
}
}
/* 3) Відомо, что подорож на Мальдіви коштує 3000$, а купити нові AirPods - 300$. Напишіть програму, яка питає у користувача число (в $) та виводить в консоль інформацію, що він за ці гроші може купити.

Якщо money === 200$, то виведе 'You can't do anything. I'm sorry :(';
Якщо money === 300$, то виведе 'You can buy only AirPods';
Якщо money === 3200.50$, то виведе 'You can go on vacation or buy AirPods! What are you waiting for?';
Якщо money === 4300.53, то виведе 'You have enough money for everything. WOW!'
 */
{
let number = parseFloat(prompt('how many dollars do you have'));

while (isNaN(number) || number < 0) {
    console.error('invalid value');
    number = parseFloat(prompt('how many dollars do you have'));
} 
switch (true) {
    case number < 299:
        console.log('You can\'t do anything. I\'m sorry :\(');
        break;
    case number > 299 && number < 3000:
        console.log('You can buy only AirPods');
        break;
    case number >= 3000 && number < 3299:
        console.log('You can go on vacation or buy AirPods! What are you waiting for?');
        break;
    case number >= 3300:
        console.log('You have enough money for everything. WOW!');
        break;
}
}
/* 4) Напишіть програму, яка питає у користувача число, виводить на екран всі числа від 1 до цього числа та возводит в степень 2 кожне ПАРНЕ його число

Якщо number === 5, то виведе '1 4 3 16 5' */
{
let number = +prompt('Enter your number > 0');

while (isNaN(number) || number <= 0) {
    console.error('invalid value');
    number = +prompt('Enter your number > 0');

}
for (let i = 1, sum = '', loopVar; ;i++) {
    loopVar = i;
    if (i > number) {
        console.log(sum);
        break;
    }
    if (i % 2 === 0) {
        loopVar = Math.pow(i, 2);
    }
    sum += loopVar + ' ';
    
}
}


/* 6) Напишіть програму, яка питає у користувача символ та число і виводить цей символ послідовно, збільшуючи кожен раз на 1, поки кількість символів в рядку не буде дорівнювати цьому числу.

Якщо symbol === #, number === 5, то виведе
#
##
###
####
##### */
{
let number = +prompt('Enter your number > 0');
let symbol = prompt('Enter your symbol');

while (isNaN(number) || number <= 0) {
    console.error('invalid value');
    number = +prompt('Enter your number > 0');

}
let numberLoop = 1;
let symbolLoop = symbol;
while (numberLoop <= number) {
    console.log(symbol);
    numberLoop++;
    symbol += symbolLoop;
}
}
/* 7) Напишіть цикл, який заповнює строку value числами від 1000 до 2000 і додайте до кожного числа символи '&#'. Результат первірте в браузері, запустив index.html файл.
Формат відповіді:
console.log(value); // &#1000 &#1001 &#1002 ... &#1999 &#2000
 */
{
let result = document.getElementById('result');
let value = '';

for (let valueNumberStart = 1000, symbolPrefix = '&#', symbolLoop = ''; ; valueNumberStart++) {
    if (valueNumberStart > 2000) {
        break;
    }
    symbolLoop = symbolPrefix + valueNumberStart + ' ';
    value += symbolLoop; 
}

result.innerHTML = value;
}
/* Перевірте також код для значень від 7000 до 10000 */
{
let result2 = document.getElementById('result2');
let value = '';

for (let valueNumberStart = 7000, symbolPrefix = '&#', symbolLoop = ''; ; valueNumberStart++) {
    if (valueNumberStart > 10000) {
        break;
    }
    symbolLoop = symbolPrefix + valueNumberStart + ' ';
    value += symbolLoop; 
}

result2.innerHTML = value;
}