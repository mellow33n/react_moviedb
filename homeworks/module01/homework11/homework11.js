/* Нагадую, що працюємо виключно через методи масиву.
1) Допрацюйте масив employee, з яким ви вже працювали раніше.
1.1) Додайте у масив два нові обʼєкти (ключі повинні бути такі ж, а значення придумайте самі). */
empolyee.push({
    id: 12,
    name: 'Elon',
    surname: 'Musk',
    salary: 100500,
    workExperience: 30,
    isPrivileges: true,
    gender: 'male',
},
{
    id: 13,
    name: 'Rey',
    surname: 'Skywalker',
    salary: 9555,
    workExperience: 2,
    isPrivileges: false,
    gender: 'female',
});
//1.2) Виведіть в консоль масив жінок-співробітниць з досвідом роботи менше ніж 10 місяців.
const employeeNewGirls = empolyee.filter((obj) => {
        return obj.gender === 'female' && obj.workExperience <= 10;
});
console.log(employeeNewGirls);

//1.3) Виведіть в консоль обʼєкт співробітника, у якого id=4
const employeeId4 = empolyee.find((obj) => {
        return obj.id === 4;

});
console.log(employeeId4);

// 1.4) Виведіть в консоль масив прізвищ співробітників.
const employeeSurnames = empolyee.map((obj) => {
    return obj.surname;
});
console.log(employeeSurnames);

/* 2) Створіть масив frameworks зі значеннями: 'AngularJS', 'jQuery'
a. Додайте на початок масиву значення 'Backbone.js' */
let frameworks = ['AngularJS', 'jQuery'];
frameworks.unshift('Backbone.js');
// b. Додайте до кінця масиву значення 'ReactJS' і 'Vue.js'
frameworks.push('ReactJS');
frameworks.push('Vue.js');
// с. Додайте в масив значення 'CommonJS' другим елементом масиву
let buffer = frameworks.shift();
frameworks.unshift('CommonJS');
frameworks.unshift(buffer);
// d. Знайдіть та видаліть із масиву значення 'jQuery' (потрібно знайти індекс елемента масиву) і виведіть його в консоль зі словами “Це тут зайве”
let wrongValue = frameworks.indexOf('jQuery');
console.log(`Це тут зайве: ${frameworks[wrongValue]}`);
frameworks.splice(wrongValue, 1);

//3) Створіть функцію removeNegativeElements, яка видаляє з вхідного масиву всі негативні числа.

function removeNegativeElements(array) {
    let newArrayNumbers = array.filter((value) => {
        return typeof value === 'string' || isNaN(value) || value >= 0;
                });
    return newArrayNumbers;
}
/* let result = removeNegativeElements([-9, 2, 3, 0, -28, 'value']); // [2, 3, 0, 'value'];
result = removeNegativeElements([-9, -21, -12]); // []
result = removeNegativeElements(['-102', 102]); // ['-102', 102]
result = removeNegativeElements([NaN, 45, -5, null]); // [NaN, 45, null] */

//4) Створіть функцію getStringElements, яка повертає вхідний масив лише зі строковими значеннями.
function getStringElements (array) {
    let stringElements = array.filter((value) => {
        return typeof value === 'string';
    });
    return stringElements;
}

let arr = [1 , true , 42 , "red" , 64 , "green" , "web" , new Date() , -898 , false];
let result = getStringElements(arr);
console.log(result);

/* 5) Напишите функцию flatArray, которая превращает массив в массиве в единый массив.
tips: вам поможет в этом метод reduce() c начальным значением [] и spread оператор.
Напишіть функцію flatArray, яка перетворює масив масивів на єдиний масив. */
function flatArray (array) {
    let reduceArray = array.reduce((a,b) => a.concat(b));
    return reduceArray;
}
let array = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
let result2 = flatArray(array); // [1, 2, 3, 4, 5, 6, 7, 8, 9]

//6) Створіть функцію myMap, яка приймає як аргумент масив та функцію, як цей масив має бути модифікований.
function myMap(arr, fn) {
    let myMapVar = arr.map(fn);
    return myMapVar;
}
function increaseElement (element) {
    return element * 2;
  }
{
let arr = [1, 2, 3, 4];
let result = myMap(arr, increaseElement); // [2, 4, 6, 8];
}
