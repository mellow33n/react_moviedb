/* 1) Напишіть функцію parseDate(), яка показує поточну дату і час чітко в заданому форматі.
для дати (Thu Jan 06 2022 22:03:07) - new Date(2022, 0, 6, 22, 3, 7) */
/* Today is: Thursday.
Current time is: 10 PM : 03 : 07 */
function parseDate(exampleDate) {
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let hours = exampleDate.getHours();
    let minutes = exampleDate.getMinutes();
    let seconds = exampleDate.getSeconds();
    let ampm = hours >= 12 ? 'PM' : 'AM'; // разделяю часы на "после" и "до" полудня
    hours = hours % 12; // перевожу в 12 часовой формат, есть нюанс с цифрой 12 (=== 0)
    hours = hours ? hours : 12; // 0 равен 12
    minutes = minutes < 10 ? '0' + minutes : minutes; // добавляю бублик для односимвольных значений
    seconds = seconds < 10 ? '0' + seconds : seconds;
    let time = hours + ' ' + ampm + ' : ' + minutes + ' : ' + seconds;
    console.log(`Today is: ${days[exampleDate.getDay()]}`);
    console.log(`Current time is: ${time}`);
}  
parseDate(new Date(2022, 0, 6, 22, 3, 7));

// для дати (Sat May 05 2018 04:15:09) - new Date(2018, 4, 5, 4, 15, 9):
parseDate(new Date(2018, 4, 5, 4, 15, 9));

//для дати (Sun Nov 30 2025 12:00:59) - new Date(2025, 11, 0, 12, 0, 0):
parseDate(new Date(2025, 11, 0, 12, 0, 0)); // 59?)


/* 2) Напишіть функцію getRandomInteger(min, max), яка повертає ціле число в заданому діапазоні чисел, які передані в аргументах функції.

 */
let randomNum = 0;
function getRandomInteger(min, max) {
    randomNum = min - 0.5 + Math.random() * (max - min + 1); 
    randomNum = Math.round(randomNum);
    return randomNum;
  }

/*   Напишіть програму, яка питає у користувача ціле число і порівнює його зі створеним числом за допомогою функції getRandomInteger.
Якщо користувач ввів невірне число, виведіть в консоль повідомлення про помилку.
Якщо числа співпадають, вивести в консоль Good work, якщо ні - Not matched. */

let userNum = +prompt('Enter a integer number');
while (isNaN(userNum) || userNum % 1 !== 0) {
    console.error('Enter a integer number');
    userNum = +prompt('Enter a integer number');
}
getRandomInteger(userNum - 1, userNum + 1);
if (userNum === randomNum) {
    console.log('Good work');
} else {
    console.log('Not matched');
}


/* 3) Напишіть функцію getDecimalNumber(arr), яка приймає в якості аргументу масив з чисел 0 або 1, і повертає число в десятичній системі еквівалентне заданому.
Наприклад, массив [0, 0, 0, 1] розглядається як 0001 і дорівнює 1.
Приклади для перевірки: */
/* getDecimalNumber([0, 0, 0, 1]) // 1
getDecimalNumber([0, 0, 1, 0]) // 2
getDecimalNumber([1, 1, 1, 1]) // 15
getDecimalNumber([1, 1, 1, 0, 0, 1]) // 57 */
/* Массив може бути довільної довжини. В масиві не повинно бути ніяких других даних окрім чисел 0 і 1. В цій задачі забороняється використовувати number.toString() чи parseInt(), давайте спробуємо написати алгоритм самостійно :) */

function getDecimalNumber(arr) {
    let result = 0, i = arr.length - 1;
    if (typeof arr !== 'object') {
        return console.error('enter a massive in args of function');
    }
    for (let value of arr) {
        if (value < 0 && value > 1) {
            return console.error('incorrect array, required data in binary system ');
        }
        result += value * getExponent(2, i);
        i--;
    }
    return result;
}
/* 4) Напишіть програму, яка питає у користувача число і ділить його на 2 стільки разів, поки воно не буде <= 50. Виведіть в консоль фінальне число і кількість операцій, які знадобились, щоб досягти цього числа.

Приклад виконання програми для number = 100500;

Initial number is: 100500;
Attempts: 11;
Final number is: 49.072265625; */

function number (userNum) {
    userNum = +prompt('Enter a number');
    let loopNum = userNum;
    for(let i = 0; ; i++) {
        if (loopNum <= 50) {
            return console.log(`Initial number is: ${userNum};\nAttempts: ${i};\nFinal number is: ${loopNum};`);
        }
        loopNum /= 2;
    }
}
number();
/* 5) Напишіть функцію getExponent(number, exponent), яка приймає два аргументи число і степінь і возводить це число в задану степінь (як Math.pow(), яле без Math.pow() i оператора **).
Вирішить задачу двома способами: через цикл і через рекурсію.
getExponent(2,3) // 8
getExponent(3,6) // 729
getExponent(0,0) // 1 */

function getExponent(number, exponent) {  // через цикл
   if(isNaN(number, exponent)) {
       return console.error('enter a number');
   }
   let loopVar = number, i;
   if (exponent === 1) {
       return number;
   }
   if (exponent === 0) {
       return 1;
   }
   for(i = 1; i < exponent; i++) {
       loopVar *= number; 
   }
   return loopVar;
}
function getExponentRecurse(number, exponent) {
    if( isNaN(number, exponent)) {
        return console.error('enter a number');
    }
    if (exponent === 1) {
        return number;
    }
    if (exponent === 0) {
        return 1;
    }
    return number * getExponentRecurse(number, exponent - 1);
}