/* 1) Напишіть програму, яка питає у користувача номер автобуса.
Якщо це номер 7, 255 или 115, тоді користувач може їхати. Виведіть в консолі "You can go".
Якщо ні - виведіть в консолі "Please wait". */

let userBusNumber = parseInt(prompt('type your bus number'));
if (userBusNumber == 7 || userBusNumber == 115 || userBusNumber == 255 ) {
    console.log('You can go');
} else if (isNaN(userBusNumber)) {
    console.log('type numbers please');
} else {
    console.log('Please wait');
}
/* 2) Напишіть програму, яка отримує від користувача число і порівнює його з числом Pi. (Math.PI)
Порівняйте, чи є це число більше ніж число Pi.
Порівняйте, чи є це число менше ніж число Pi.
Порівняйте, чи це число дорівнює числу Pi.
Якщо введене значення не є числов, виведіть в консоль повідомлення про помилку.
Всі результати відобразіть в наступному форматі:
You entered: <number> 
Is greater then PI: true
Is less then PI: false
Is equal PI: false */

let userNumber = parseFloat(prompt('type your number to equal with PI'));
let greaterThenPI = false;
let lessThenPI = false;
let equalThenPI = false;

if (isNaN(userNumber) === false) {
    if (userNumber > Math.PI) {
        greaterThenPI = true;
    } else if (userNumber < Math.PI) {
        lessThenPI = true;
    } else if (equalThenPI === Math.PI) {
        equalThenPI = true;
    }
}

console.log(`You entered: ${userNumber} 
Is greater then PI: ${greaterThenPI}
Is less then PI: ${lessThenPI}
Is equal PI: ${equalThenPI}`);

/* 3) Напишіть програму, яка пропонує користувачу ввести пароль і перевіряє, чи є ций пароль надійним за наступними умовами:

Пароль повинен бути не менше шести символів;
Пароль не повинен бути рівним строкам qwerty чи 123456;
Пароль повинен мати хоча б одну велику літеру.
Якщо всі умови виконані, виведіть в консоль повідомлення "Strong".

Якщо пароль має хоча б одну велику літеру але складається з п'яти символів, виведіть в консоль повідомлення "Middle".

У всіх інших випадках, виведіть в консоль повідомлення "Weak". */

let userPassword = prompt('create a password');
let userPasswordGrade = 'Weak';

if (userPassword !== '123456' && userPassword !== 'qwerty' && userPassword.length >= 6 && userPassword !== userPassword.toLocaleLowerCase()) {
    userPasswordGrade = 'Strong';
} else if (userPassword.length == 5 && userPassword !== userPassword.toLocaleLowerCase()) {
    userPasswordGrade = 'Middle';
}

console.log(userPasswordGrade);

/* 4) Напишіть програму, яка питає у користувача номер квартири (команда prompt()) і виводить в консоль номер поверху і номер під'їзду.
Відомо, що в одному під'їзді 9 поверхів по 3 квартири на кожному поверсі. Результат (поверх і під'їзд) відобразіть в консолі (команда console.log()). */
const roomsOnFloor = 3;
const floors = 9;
const roomNumber = parseInt(prompt('type your room number'));
// Нехай у домі нескінчена кількість парадних

const roomsPerEntrance = roomsOnFloor * floors; //27

// рішення для переважної більшості номерів, виключенням є кратно до поверху(3) та до парадної(27)
let userEntrance = roomNumber / roomsPerEntrance + 1; 
let userFloor =  (roomNumber % roomsPerEntrance) / roomsOnFloor + 1; 

// рішення для кратності до поверху(3)
if (roomNumber % roomsOnFloor === 0) {
    userFloor = (roomNumber % roomsPerEntrance) / roomsOnFloor;
}

// рішення для кратності до парадної(27)
if (roomNumber % roomsPerEntrance === 0) {
    userEntrance = roomNumber / roomsPerEntrance;
    userFloor = floors; //это всегда будет последний этаж
}
// засекречую данні про підземні рівні 
if (roomNumber <= 0) {
    console.log('type correct number [1 - Infinity]');
} else {
    console.log(`entrance: ${parseInt(userEntrance)}`);
    console.log(`floor: ${parseInt(userFloor)}`);
}
/* 5) Напишіть програму, яка питає у користувача температуру в Цельсіях и конвертує її в Фаренгейти. Результат відобразіть в консолі.
Формула для конвертації: Цельсій х 1,8 + 32 = Фаренгейт
tips: градус Цельсія в юнікоді буде "\u2103", щоб відобразити його в строці. Спробуйте самостійно знайти символ для позначення градусу Фаренгейта. */

let celsius = parseFloat(prompt('type Celcium temperature'));
let celsiusToFahrenheit = celsius * 1.8 + 32;
console.log(`${celsius.toFixed(1)}\u2103 is ${celsiusToFahrenheit.toFixed(1)}\u2109`);

