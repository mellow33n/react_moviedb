// Homework form lesson 02 
// Variables, alert, prompt, console.log, typeof

// 1) Напишите программу, которая спрашивает у пользователя сколько ему лет (команда prompt()) и выводит его возраст на экран (команда alert()) 
let userAge = prompt('how old are you?');
alert(`wow, you are ${userAge} years old`);

// 2) Напишите программу, которая спрашивает у пользователя как его зовут, в какой стране и в каком городе он живет (команда prompt()) и выведите результат в консоль в следующем виде (команда console.log())

let userName = prompt('what is your name?');
let userFromCountry = prompt('which country are you from?');
let userFromCity = prompt('and one more question, where exactly?');
console.log(`Hello, your name is ${userName}. You live in ${userFromCity}, ${userFromCountry}.`);

/* 3) Напишите программу, которая выводит в консоль (команда console.log()) тип 3х разных переменных.

    3.1) Создайте переменную number и присвойте ей значение 156798;
    3.2) Создайте переменную string и присвойте ей значение IT School Hillel;
    3.3) Создайте переменную boolean и присвойте ей значение false;
    3.4) Отобразите значение каждой переменной и её тип в консоли в следующем виде:

    value: <value>; type: <type> */

let number = 156798;
let string = 'IT School Hillel';
let boolean = false;
console.log(`value: ${number}; type:`, typeof number);
console.log(`value: ${string}; type:`, typeof string);
console.log(`value: ${boolean}; type:`, typeof boolean);
