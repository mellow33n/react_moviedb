import { Container, Row, Col } from "react-bootstrap";

export default function StepHeader({children}) {
    return (
        <div className="App">
          <Container>
            <Row>
              <Col  md={{ span: 6, offset: 3 }} className="custom-margin">
                {children}
              </Col>
            </Row>
          </Container>
        </div>
    )
}