import { useState, useContext } from "react";
import StepOne from "./Components/First";
import StepTwo from "./Components/Second";
import Final from "./Components/Finito";
import StepHeader from "./stepHeader";
import { FormContextComponent } from "./Components/context";
import { HANDLE_INPUT_DATA } from "./Components/reducer";
import { StepThree } from "./Components/Third";

export default function App() {
  const [formData, dispatch] = useContext(FormContextComponent);
  const [step, setStep] = useState(1);

  const nextStep = () => {
    setStep(step + 1);
  };

  const prevStep = () => {
    setStep(step - 1);
  };

  const handleInputData = input => e => {
    const { value } = e.target;
    dispatch({
      type: HANDLE_INPUT_DATA,
      payload: { input, value }
    });
  }

  switch (step) {
    case 1:
      return (
        <StepHeader>
          <StepOne nextStep={nextStep} handleFormData={handleInputData} />
        </StepHeader>
      );
    case 2:
      return (
        <StepHeader>
          <StepTwo nextStep={nextStep} prevStep={prevStep} handleFormData={handleInputData} />
        </StepHeader>
      );
    case 3:
      return (
        <StepHeader>
          <StepThree prevStep={prevStep} nextStep={nextStep}/>
        </StepHeader>
      );
    case 4:
      return (
        <StepHeader>
          <Final values={formData} />
        </StepHeader>
      );
    default:
      return (
        <div className="App">
        </div>
      );
  }
}

