
export const initialState = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
    image: 'https://www.meme-arsenal.com/memes/727769b960090ad6f99c5f13eaaa7ab0.jpg',

};
export const HANDLE_INPUT_DATA = '[FORM] HANDLE INPUT DATA';

export const pageReducer = (state = initialState, action) => {
    const input = action.payload.input;

    switch (action.type) {
        case HANDLE_INPUT_DATA:
            return {
                ...state,
                [input]: action.payload.value       
            }              
        default:
            break;
    }
}

