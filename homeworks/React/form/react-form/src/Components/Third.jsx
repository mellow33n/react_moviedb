import { createRef, useState, useContext } from "react";
import { Button, Form, Alert } from "react-bootstrap";
import { FormContextComponent } from "./context";
import { HANDLE_INPUT_DATA } from "./reducer";
import "./Third.css";

export const StepThree = ({ prevStep, nextStep }) => {
  const [, dispatch] = useContext(FormContextComponent);
  const [img, setImg] = useState({
    src: "",
    name: "",
  });
  const fileInputRef = createRef();
  function choseFile() {
    fileInputRef.current.click();
  }
  const updateImage = (event) => {
    const reader = new FileReader();
    const file = event.target.files[0];

    if (file) {
      reader.readAsDataURL(file);
    }

    reader.onloadend = () => {
      const value = reader.result;
      const input = "image";
      dispatch({
        type: HANDLE_INPUT_DATA,
        payload: { input, value },
      });

      setImg({
        src: reader.result,
        name: file.name,
      });
    };
  };
  const submitFormData = (e) => {
    e.preventDefault();
    nextStep();
  };

  return (
    <>
      <Form onSubmit={submitFormData}>
        <Form.Group className="mb-3" controlId="formBasicPassword">
        <Alert key={'light'} variant={'light'}>
          Avatar
        </Alert>
          <div className={"img-upload-button"}>
            <Button variant="outline-primary" size="lg" onClick={choseFile}>
              Upload image
            </Button>
            <Form.Control
              type="file"
              style={{ display: "none" }}
              onChange={updateImage}
              ref={fileInputRef}
            />
            <img src={img.src} alt={img.name} />
            <p>{img.name}</p>
          </div>
        </Form.Group>
        <div className={"footer-buttons"}>
          <Button variant="primary" onClick={prevStep}>
            Previous
          </Button>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </div>
      </Form>
    </>
  );
};
