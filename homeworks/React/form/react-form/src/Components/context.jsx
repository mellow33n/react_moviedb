import { createContext, useReducer } from "react";

import { pageReducer, initialState } from './reducer'




export const FormContextComponent = createContext({});

export function FormContext({children}) {
    const [formData, dispatch] = useReducer(pageReducer, initialState);
    return (
        <FormContextComponent.Provider value={[{formData}, dispatch]}>
            {children}
        </FormContextComponent.Provider>
    )

}