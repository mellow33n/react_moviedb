import { useContext } from "react";
import { Card, Alert } from "react-bootstrap";
import { FormContextComponent } from "./context";

const Final = () => {
  const [{ formData }] = useContext(FormContextComponent);
  return (
    <>
    <Alert key={'light'} variant={'light'}>
          Conclusion
        </Alert>
      <Card style={{ marginTop: 100, textAlign: "left" }}>
        <Card.Body>
          <p>
            <strong>First Name :</strong> {formData.firstName}{" "}
          </p>
          <p>
            <strong>Last Name :</strong> {formData.lastName}{" "}
          </p>
          <p>
            <strong>Email :</strong> {formData.email}{" "}
          </p>

          <img src={formData.image} alt="" />
        </Card.Body>
        <Alert key={"success"} variant={"success"}>
          Registration done! Thank you!
        </Alert>
      </Card>
    </>
  );
};

export default Final;
