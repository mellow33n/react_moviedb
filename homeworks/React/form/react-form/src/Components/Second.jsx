import React, { useState, useContext } from "react";
import { Form, Card, Button, Alert } from "react-bootstrap";
import { FormContextComponent } from "./context";

const StepTwo = ({ nextStep, handleFormData, prevStep }) => {
  const [{ formData }] = useContext(FormContextComponent);
  const [error, setError] = useState(false);

  const submitFormData = (e) => {
    e.preventDefault();
    if (
      formData.password !== formData.confirmPassword
    ) {
      setError(true);
    } else {
      nextStep();
    }
  };


  return (
    <>
      <Card style={{ marginTop: 100 }}>
        <Card.Body>
        <Alert key={'light'} variant={'light'}>
          Password
        </Alert>
          <Form onSubmit={submitFormData}>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                defaultValue={formData.password}
                onChange={handleFormData("password")}
              />
              
            </Form.Group>
            <Form.Group className="mb-3" controlId="formConfirmPassword">
              <Form.Label>Confirm password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Confirm password"
                defaultValue={formData.confirmPassword}
                onChange={handleFormData("confirmPassword")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  Password does not match
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
              <Button variant="primary" onClick={prevStep}>
                Previous
              </Button>

              <Button variant="primary" type="submit">
                Next
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </>
  );
};

export default StepTwo;