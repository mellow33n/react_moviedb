import { useEffect } from 'react';
import './App.scss';
import { useSelector, useDispatch } from 'react-redux';
import { loadCharacters } from '../Reducer/actions';
import HogwartsHouses from '../Houses/Houses';
import Search from '../Search/Search';
import CharactersList from '../CharList/CharactersList';



function App() {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  useEffect(() => {
    const loadChars = async () => {
      try {
        const res = await fetch('https://hp-api.herokuapp.com/api/characters');
        const resJSON = await res.json()
        return resJSON
      } catch (err) {
        console.error(err);
      }
    }
    loadChars().then((res) => dispatch(loadCharacters(res)))
  }, [dispatch]);

  return (
    <>
      <div className="container">
        <h1>&#x2728;Harry Potter Characters &#x2728;</h1>
        <HogwartsHouses></HogwartsHouses>
        <Search></Search>
        <CharactersList characters={state}></CharactersList>

      </div>
    </>
  );
}

export default App;
