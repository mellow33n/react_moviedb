import { LOAD_CHARACTERS, SEARCH_CHARACTERS, FILTER_HOUSE } from "./actions";


export const initialState = {
    characters: [],
    filtredCharacters: [],

}

export const charReducer = function (state = initialState, action) {
    switch (action.type) {
        case LOAD_CHARACTERS:
            return {
                ...state,
                characters: action.payload,
                filtredCharacters: action.payload,

            }
        case SEARCH_CHARACTERS:
            return {
                ...state,
                filtredCharacters: state.characters.filter((value) => value.name.toLowerCase().includes(action.payload)),
            }
        case FILTER_HOUSE:
            return {
                ...state,
                filtredCharacters: state.characters.filter((value) => value.house.includes(action.payload)),
            }
        default:
            break;
    }
}
