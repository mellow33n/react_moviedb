export const LOAD_CHARACTERS = 'Load';
export const SEARCH_CHARACTERS = 'Search';
export const FILTER_HOUSE = 'Filter';

export const loadCharacters = (characters) => ({
    type: LOAD_CHARACTERS,
    payload: characters,
});
export const searchCharacters = (value) => ({
    type: SEARCH_CHARACTERS,
    payload: value,
});
export const filterHouse = (value) => ({
    type: FILTER_HOUSE,
    payload: value,
});






