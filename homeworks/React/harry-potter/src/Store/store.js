import { createStore, applyMiddleware, compose } from "redux";
import { charReducer } from "../Reducer/reducer";
import { middlewares } from "./middlewares";

const middlewareEnhancer = applyMiddleware(...middlewares);
const composeEnhancer = compose(middlewareEnhancer);

export const store = createStore(charReducer, composeEnhancer);
