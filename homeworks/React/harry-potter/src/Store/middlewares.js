import { createLogger } from 'redux-logger';

const logger = createLogger();

const middlewares = [];

if (process.env.NODE_ENV === 'development') {
  middlewares.push(logger);
}

export { middlewares };
