
import { useDispatch } from "react-redux";
import { searchCharacters } from "../Reducer/actions";

export default function Search() {
  const dispatch = useDispatch();
  return (
    <div id="searchWrapper">
      <input
        type="text"
        name="searchBar"
        id="searchBar"
        onChange={(e) => dispatch(searchCharacters(e.target.value))}
        placeholder="search for a character"
      />
    </div>
  );
}
