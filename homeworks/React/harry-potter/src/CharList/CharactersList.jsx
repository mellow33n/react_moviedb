export default function CharactersList({ characters }) {
  if (characters) {
    const arrFilterData = characters.filtredCharacters;
    

    return (
      <ul id="charactersList">
        {arrFilterData.map((item, i) => (
          <li className="character" key={i}>
            <h2>{item.name}</h2>
            <p>House: {item.house}</p>
            <img src={item.image ? item.image : require('./empty.png') } alt={item.name}></img>
          </li>
        ))}
      </ul>
    );
  }
}
