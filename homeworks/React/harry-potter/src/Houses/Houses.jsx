
import { useDispatch } from "react-redux";
import { filterHouse } from "../Reducer/actions";

export default function HogwartsHouses() {
    const dispatch = useDispatch();
    
  
  const houses = ["Gryffindor", "Slytherin", "Ravenclaw", "Hufflepuff"];
  function handleSelectHouse(house) {
    dispatch(filterHouse(house))
  }
  return (
    <div className="hogwarts-houses">
      {houses.map((house, i) => (
        <div className="house" key={i} onClick={() => handleSelectHouse(house)}>
          <img src={`houses/${house}.png`} alt={house} />
        </div>
      ))}
    </div>
  );
}
