import { Fragment } from "react";
import SongList from "../songList/SongList";
import Header from "../header/Header";
import Sort from "../sort/Sort";
import Counter from "../counter/Counter";
import LoginButton from "../Login/LoginButton";
import FavouriteSongs from "../Fav/FavoriteSongsBtn";

export default function Main() {
    

    return <Fragment>
    <LoginButton ></LoginButton>
    <Header></Header>
    <Sort></Sort>
    <FavouriteSongs></FavouriteSongs>
    <SongList></SongList>
    <Counter></Counter>
  </Fragment>
}