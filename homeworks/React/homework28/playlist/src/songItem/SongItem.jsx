import { PlaylistContextComponent } from "../context/Context";
import { useContext } from "react";
import { REMOVE_SONG, LIKE_SONG } from "../reducer/PlaylistReducer";

export default function SongItem({song}) {
    const [, dispatch] = useContext(PlaylistContextComponent);

    function removeSong(id) {
      dispatch({
        type: REMOVE_SONG,
        payload: { id },
      })
    }
  
    function likeSong(id) {
      dispatch({
        type: LIKE_SONG,
        payload: { id },
      })
    }

    return (
        <div className='song-item' id={song.id} key={song.id}>
          <img src ="https://svgshare.com/i/jhk.svg" className={song.isLiked ? '' : 'unlike'} alt=""></img>
          <button className='like-button' onClick={()=>likeSong(song.id)}>
            {song.isLiked ? 'Unlike' : 'Like'}
            </button>
          <p>{song.name}</p>
          <button className='remove-button' 
          onClick={()=> removeSong(song.id)}>
            Remove
            </button>
          </div>
    );
}