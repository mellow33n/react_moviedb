import { createContext, useReducer } from "react";

import { PlaylistReducer, initialState } from "../reducer/PlaylistReducer";




export const PlaylistContextComponent = createContext({});

export function PlaylistContext({children}) {
    const [{songs}, dispatch] = useReducer(PlaylistReducer, initialState);

    return (
        <PlaylistContextComponent.Provider value={[{songs}, dispatch]}>
            {children}
        </PlaylistContextComponent.Provider>
    )

}