



export const initialState = {
    songs: [],
}
export const ADD_SONG = '[SONG] ADD Song';
export const ADD_SONGS = '[SONG] ADD Songs';
export const REMOVE_SONG = '[SONG] REMOVE Song';
export const LIKE_SONG = '[SONG] LIKE Song';
export const SORT_SONG = '[SONG] SORT Song';
export const FAV_SONGS = '[SONG] FAV Song';
let flag = false;

export const PlaylistReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_SONG:
            return {
                ...state,
                songs: [action.payload.song, ...state.songs],
            };
        case REMOVE_SONG:
            return {
                ...state,
                songs: state.songs.filter((value) => value.id !== action.payload.id)
            };
        case LIKE_SONG:
            const indexSong = state.songs.findIndex((value) => value.id === action.payload.id);
            return {
                ...state,
                songs: Array.from(state.songs, (value, index) =>
                    indexSong === index ? {
                        name: value.name,
                        isLiked: !value.isLiked,
                        id: value.id
                    }
                        : value
                )
            };
        case SORT_SONG:
            flag = !flag
            return {
                ...state,
                songs: flag ? state.songs.sort((a, b) =>
                    a.isLiked === b.isLiked ? 0 : a.isLiked ? -1 : 1
                )
                    : state.songs.sort((a, b) =>
                        a.isLiked === b.isLiked ? 0 : a.isLiked ? 1 : -1
                    )
            }
        case ADD_SONGS:
            return {
                ...state,
                songs: action.payload
                
            }
        case FAV_SONGS:
            return {
                ...state,
                songs: state.songs.filter((value) => value.isLiked)
            }
        default:
            return state;
    }
}