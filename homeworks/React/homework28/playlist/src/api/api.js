import axios from "axios";

const baseHSconfig = axios.create({
  baseURL: `http://localhost:4000`,
});

 export async function getPlaylist() {
  const data = await baseHSconfig.get("/playlist");
  return data.data;
}
export const initPlaylist = getPlaylist();
