
import { useNavigate } from "react-router-dom";

export default function FavouriteSongsButton() {
  const navigate = useNavigate();
  const currentRoute = window.location.pathname;
  function handleFavSongs() {
    switch (currentRoute) {
      case "/favorites":
        navigate('/songs');
        break;
      case "/songs":
        navigate('/favorites');
        break;
      default:
        break;
    }
  }


  return (
    <button onClick={handleFavSongs}>
      {currentRoute === "/favorites" ? "All songs" : "Favorite songs"}
    </button>
  );
}
