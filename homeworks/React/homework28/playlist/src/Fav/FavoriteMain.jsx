import { Fragment } from "react";
import Counter from "../counter/Counter";
import LoginButton from "../Login/LoginButton";
import FavouriteSongsButton from "./FavoriteSongsBtn";
import FavoriteSongList from "./FavoriteSongList";

export default function FavoriteMain() {
    return <Fragment>
    <LoginButton ></LoginButton>
    <h2>Favorite songs</h2> 
    <FavouriteSongsButton></FavouriteSongsButton>
    <FavoriteSongList></FavoriteSongList>
    <Counter></Counter>
  </Fragment>
}