import { Fragment, useContext, useEffect } from "react";
import SongItem from "../songItem/SongItem";
import { PlaylistContextComponent } from "../context/Context";

import { FAV_SONGS } from "../reducer/PlaylistReducer";

export default function FavoriteSongList() {
  const [{ songs }, dispatch] = useContext(PlaylistContextComponent);
  console.log(songs);
  useEffect(() => {
    dispatch({
      type: FAV_SONGS,
    });
  }, [dispatch]);

  return (
    <Fragment>
      {songs.map((song) => (
        <SongItem className="song-item" song={song} key={song.id}></SongItem>
      ))}
    </Fragment>
  );
}
