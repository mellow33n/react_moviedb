import { useNavigate } from "react-router-dom";

export default function NotFound() {
    const nav = useNavigate();
  function handleSubmit(event) {
    event.preventDefault();
    nav('/login');
  }
    return (
        <div>
    <h1>404 Not found any data</h1>
    <button onClick={handleSubmit}>Log in</button>
    </div>
    )
}