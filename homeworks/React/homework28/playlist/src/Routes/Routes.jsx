import { Navigate } from "react-router-dom";
import { lazy, Suspense } from "react";
//import Main from "../Main/Main";

const Main = lazy(() => import("../main/Main"));
const FavoriteMain = lazy(() => import("../Fav/FavoriteMain"));
const Login = lazy(() => import("../Login/Login"));
const NotFound = lazy(() => import("../404/NotFound"));

function getComponent(Component) {
  const TOKEN = localStorage.getItem("AUTH_TOKEN");
  return TOKEN ? (
    <Suspense>
      <Component />
    </Suspense>
  ) : (
    <Navigate to="/login" />
  );
}

export const routes = [
  {
    path: "/songs",
    element: getComponent(Main),
  },
  {
    path: "/favorites",
    element: getComponent(FavoriteMain),
  },
  {
    path: "login",
    element: (
      <Suspense>
        <Login />
      </Suspense>
    ),
  },

  {
    path: "*",
    element: getComponent(NotFound),
  },
];
