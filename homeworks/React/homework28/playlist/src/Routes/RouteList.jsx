import { useRoutes } from "react-router-dom";
import { routes } from "./Routes";

export default function RoutesList() {
  let element = useRoutes(routes);

  return element;
}
