import { useNavigate } from "react-router-dom";

export default function LoginButton() {
    const TOKEN = localStorage.getItem("AUTH_TOKEN");

  const nav = useNavigate();
  function handleSubmit(event) {
    event.preventDefault();
    nav('/login');
  }

  return (
    <button className="btn-login" onClick={handleSubmit}>
        {TOKEN ? 'Log out' : 'Log in'}
    </button>
  );
}
