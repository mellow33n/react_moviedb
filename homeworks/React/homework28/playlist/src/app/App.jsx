import "./App.css";
import { BrowserRouter} from "react-router-dom";
import RoutesList from "../Routes/RouteList";

function App() {

  return (
    <BrowserRouter>
      <RoutesList />
    </BrowserRouter>
  );
}
export default App;
