import { useContext } from "react";
import { PlaylistContextComponent } from "../context/Context";

export default function Counter() {
  const [{songs}] = useContext(PlaylistContextComponent);
    return <div>
    <p>Count of songs: {songs.length}</p>
      </div>
}