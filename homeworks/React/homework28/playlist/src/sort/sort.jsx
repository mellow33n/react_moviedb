import { Fragment, useContext } from "react";
import { PlaylistContextComponent } from "../context/Context";
import { SORT_SONG } from "../reducer/PlaylistReducer";


export default function Sort() {
    const [, dispatch] = useContext(PlaylistContextComponent);
    function sortSongs() {
        dispatch({
          type: SORT_SONG,
          payload: {},
        })
      }
    
    return <Fragment>
        <button onClick={() => {
            sortSongs()
        }}>
            Sort
            </button>
    </Fragment>
}