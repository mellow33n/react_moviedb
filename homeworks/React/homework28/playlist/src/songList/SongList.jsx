import { Fragment, useContext, useEffect } from "react";
import SongItem from "../songItem/SongItem";
import { PlaylistContextComponent } from "../context/Context";
import { getPlaylist } from "../api/Api";
import { ADD_SONGS } from "../reducer/PlaylistReducer";



export default function SongList() {
  const [{songs}, dispatch] = useContext(PlaylistContextComponent);
  useEffect(() => {
    getPlaylist().then((songsList) => dispatch({
      type: ADD_SONGS,
      payload: songsList,
    }))
   }, [dispatch])

  return (
        <Fragment>
        {songs.map((song) => (
          <SongItem className="song-item" song={song} key={song.id}></SongItem>
        ))}
        </Fragment>
  );
}
