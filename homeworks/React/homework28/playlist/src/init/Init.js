import { v4 as uuidv4 } from 'uuid';

export const initSongs = [
  {
    name: "Jingle Bells",
    isLiked: false,
    id: uuidv4()
  },
  {
    name: "We Wish You a Merry Christmas",
    isLiked: false,
    id: uuidv4()
  },
  {
    name: "Bohemian Rhapsody",
    isLiked: true,
    id: uuidv4()
  },
  {
    name: "Civilian",
    isLiked: true,
    id: uuidv4()
  }
];

