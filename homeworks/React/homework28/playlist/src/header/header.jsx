import { Fragment, useState, useContext } from "react"
import { v4 as uuidv4 } from 'uuid'
import { PlaylistContextComponent } from "../context/Context";
import { ADD_SONG } from "../reducer/PlaylistReducer";


export default function Header() {
    const [, dispatch] = useContext(PlaylistContextComponent);

    
    function addNewSong(song) {
        dispatch({
          type: ADD_SONG,
          payload: { song },
        })
      }
    
    const [newSong, setNewSong] = useState('');
    const onChange = function (event) {
        setNewSong(event.target.value);
    }
    const onSubmit = function (event) {
        event.preventDefault()
        const song = {
            name: newSong,
            isLiked: false,
            id: uuidv4()
        }
        addNewSong(song)
    }

    return <Fragment>
        
        <div className="header">
        <h2>Playlist</h2>
        
        <form action="" onSubmit={onSubmit}>
        <input 
            className="input-box" 
            value={newSong}
            onChange={onChange}    
            placeholder="Song..." 
        />
        <button className="button-add" >Add song</button>
        </form>
        </div>
    </Fragment>
}