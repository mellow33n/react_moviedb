//1) Створіть 3 змінні з наступними значеннями:

let string1 = "Aquamarine Black Blue Brown Chocolate ";
let string2 = "Green Lime Olive Orange Purple ";
let string3 = "Red Tomato Violet White Yellow";

//а) Створіть функцію joinColor(), яка обʼєднює всі 3 строки в одну. Функція повинна працювати з будь-якою кількістю вхідних строк.
function joinColor(...args) {
    let result = args.toString();
    return result.replaceAll(',', '');
}
let colors = joinColor(string1, string2, string3); // Aquamarine Black Blue Brown Chocolate Green Lime Olive Orange Purple Red Tomato Violet White Yellow

/* b) Створіть функцію indexColor(), яка повертає індекс першого входження вхідної строки в строці. Функція приймає два параметри:
        строку, в якої виконується пошук (наприклаж colors);
        строку, індекс якої, потрібно знайти */

function indexColor(str, indexStr) {
    return str.indexOf(indexStr);
}
let greenIndex = indexColor(colors, 'Green'); // 38

//c) Створіть функцію isColorIncludes(), яка перевіряє чи є такий колір в заданій строці чи ні. Зверніть увагу, що функція повинна працювати з будь-яким регістром.

function isColorIncludes(str, indexStr) {
    str = str.toLowerCase();
    indexStr = indexStr.toLowerCase();
    return str.includes(indexStr);
}

console.log(isColorIncludes(colors, 'Black')); // true
console.log(isColorIncludes(colors, 'BlAcK')); // true
console.log(isColorIncludes(colors, 'Lilac')); // false

// d) Створіть функцію replaceColor(), яка знаходить строку в заданій строці і замінює іі на іншу

function replaceColor(str, findValue, replaceValue) {
    return str.replace(findValue, replaceValue);
}
let result = replaceColor(string2, 'Olive', 'Grey'); // "Green Lime Grey Orange Purple "

/* e) Створіть функцію splitColors(colors, numbers), яка розбиває вхідну строку на окремі слова і повертає тільки ті слова, кількість яких не менше ніж число, яке передане в якості другого аргументу функції. */
function splitColors(str, number) {
    let splittedArr = str.split(' ');
    let strNumberLength = splittedArr.map((value) => value.length > number ? value + ' ': ',');
    return joinColor(strNumberLength).trim();
}
let filteredColors = splitColors(colors, 6); // "Aquamarine Chocolate"

//f) Створіть функцію calculateSpaces(), яка рахує кількість пробілів в строці.
function calculateSpaces (str) {
    return str.split(' ').length - 1;    
}
console.log(string1);
let count = calculateSpaces(string3); // 4