
/* Создайте класс Student который принимает в качестве аргумента в конструкторе объект enrollee (абитурент). У экземпляра класса Student должны быть поля:

id - уникальный идентификатор студента (генерируется при создании экземпляра и начинается с 1);
name - имя студента (передаем в объекте enrollee);
surname - фамилия студента (передаем в объекте enrollee);
ratingPoint - рейтинг студента по результатам вступительных экзаменов (передаем в объекте enrollee);
schoolPoint - рейтинг студента по результатам ЗНО (передаем в объекте enrollee);
isSelfPayment - если true, то студент на контракте, если false - на бюджете (генерируется по логике указанной ниже).
tips: для генерации id, можно создать отдельное статическое свойство в классе (static id = 1) и обращаться потом к нему в любом месте класса (Student.id) */

class Student {
    static id = 0;
    constructor(name, surname, ratingPoint, schoolPoint, isSelfPayment){
        this.id = ++Student.id;
        this.name = name;
        this.surname = surname;
        this.ratingPoint = ratingPoint;
        this.schoolPoint = schoolPoint;
        this.isSelfPayment = isSelfPayment;
    }
}
const students = [];
for (let enr of enrollee) {
    students.push(new Student(enr.name, enr.surname, enr.ratingPoint, enr.schoolPoint, enr.isSelfPayment)) 
}
/* Создайте класс University со свойством name и методом addStudent().
Метод addStudend() должен реализовывать следующую логику:
Добавлять студентов в массив студентов университета;
Если у студента ratingPoint больше или равен 800, то студент может быть зачислен на бюджет, при условии что он будет входить в пятерку (5) лучших студентов рейтинга (напомню что рейтинг считается как сумма результатов ЗНО и вступительных экзаменов - schoolPoint и ratingPoint);
Устанавливать правильное значение isSelfPayment для каждого студента;
Добавьте так же методы для получения данных о списках зачисленных студентов и студентов, которые зачислены на бюджет. */


class University {
    constructor(name) {
        this.name = name;
        this.students = [];
    }
    addStudent(student) {
        this.students.push(student);
        this.studentsByRating;
        this.setGrants();
    }
    get studentsByRating() {
        this.students.sort((a,b) => {
            if (a.schoolPoint+a.ratingPoint < b.schoolPoint + b.ratingPoint) {
                return 1;
            }
            if (a.schoolPoint+a.ratingPoint > b.schoolPoint + b.ratingPoint) {
                return -1;
            }
            return 0;
        })
    }
    setGrants() {
        this.students.map((value, index) => {
            if (value.ratingPoint > 800 && index < 5) {
                value.isSelfPayment = false;
            } else {
                value.isSelfPayment = true;
            }
        })
    }
    getStudentsList() {
        return console.log(this.students)
    }
    getStudentsGrantsList() {
        const grantStudents = this.students.filter((value, index) => index < 5 ? true : false)
        return console.log(grantStudents)
    }
}

const university = new University('Жашківській національний тракторно-моторний універсітет')
for (let add of students) {
    university.addStudent(add);
}
university.getStudentsGrantsList();
university.getStudentsList();



