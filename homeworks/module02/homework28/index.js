/* eslint-disable linebreak-style */
/* eslint-disable no-console */
/* eslint-disable linebreak-style */
/* eslint-disable arrow-parens */
/* eslint-disable linebreak-style */
/* eslint-disable no-trailing-spaces */
/* eslint-disable linebreak-style */
/* eslint-disable max-len */
/* eslint-disable linebreak-style */
/* eslint-disable prefer-const */
/* eslint-disable spaced-comment */
/* eslint-disable indent */
/* eslint-disable linebreak-style */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable linebreak-style */
/* eslint-disable no-var */
/* eslint-disable linebreak-style */
/* eslint-disable semi */
/* eslint-disable linebreak-style */
/* eslint-disable import/newline-after-import */
/* eslint-disable linebreak-style */
/* eslint-disable no-multiple-empty-lines */
// eslint-disable-next-line linebreak-style

const express = require('express');
const util = require('util');
const fs = require('fs');
var bodyParser = require('body-parser')
const app = express();
const PORT = 3000;
const { v4: uuidv4 } = require('uuid');

const readFileAsPromise = util.promisify(fs.readFile);
const writeFileAsPromise = util.promisify(fs.writeFile);

// parse application/json
app.use(bodyParser.json())


//Ваш сервер повинен мати наступні ендпоінти (api call або роути) згідно REST:
//GET /notes - повертає массив усіх нотаток, які знаходяться в json файлі;
app.get('/notes', async (req, res) => {
    const data = await readFileAsPromise('./data/notes.json', 'utf-8');
    res.send(data);
});


//POST /notes - отримує дані в тілі запиту, генерує id і на основі цих даних створює новий об'єкт нотатки і додає його до json файлу;
app.post('/notes', async (req, res) => { 
    console.log(req.body);
    await writeFileAsPromise('./random.json', JSON.stringify(req.body));
    //записываю уже имеющиеся заметки
    const mainData = await readFileAsPromise('./data/notes.json', 'utf-8');
    //сохраняю значение из body
    const bodyData = await readFileAsPromise('./random.json', 'utf-8');
    //формирую объект
    let bodyDataParse = JSON.parse(bodyData);
    bodyDataParse.id = uuidv4();
    bodyDataParse.dateCreated = new Date();
    bodyDataParse.isShared = false;
    //склеиваю необходимые данные
    let mainDataParse = JSON.parse(mainData);
    mainDataParse.push(bodyDataParse);
    await writeFileAsPromise('./data/notes.json', JSON.stringify(mainDataParse));

    res.status(201);
    res.send('Success');
});
//DELETE /notes/:id - видаляє замітку, по переданому id в параметрах запиту;
app.delete('/notes/:id', async (req, res) => {
  const mainData = await readFileAsPromise('./data/notes.json', 'utf-8');
  let mainDataParse = JSON.parse(mainData);
  let index = mainDataParse.findIndex(item => item.id === req.params.id)
  mainDataParse.splice(index, 1);
  await writeFileAsPromise('./data/notes.json', JSON.stringify(mainDataParse));
  res.status(201);
  res.send('Success');
});




app.listen(PORT, () => {
  console.log(`Example app listening on port ${PORT}`)
})
