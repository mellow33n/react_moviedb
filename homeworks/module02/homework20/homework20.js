/* 1) Создайте Promise, которое должно выполнять следующие действия:
        через 2 секунды он должен создать переменную number со случайным значением от 1 до 6;
        если 1 <= number <= 5, вывести в консоли сообщение "Start the game..." и вернуть число;
        если number = 6, вернуть ошибку; */

function randomInteger(min, max) {
    return Math.floor(min + Math.random() * (max + 1 - min));
}
const myFirstPromise = new Promise((res, rej) => {
    setTimeout(() =>{
        let randomNumber = randomInteger(1, 6);
        if(1 <= randomNumber && randomNumber <= 5) {
            console.log('Start the game...');
            res(randomNumber);
        }
        if(randomNumber === 6) {
            rej(randomNumber);
        }
    },2000);
});

/* Затем должна быть запущена функция-потребитель (than()), которая выполняет следующие

действия:

если number = 1, вывести сообщение "Stay here" ;
если number >= 2, вывести сообщение "Go <number> steps" .
Используйте catch(), чтобы отловить ошибку и выведите сообщение "Exit" . */
myFirstPromise
.then((value) => value === 1 ? console.log('Stay here') : console.log(`Go ${value} steps`))
.catch((error) => console.error(error));

/* 2) Создайте функцию goToShop(), которая возвращает успешный Promise с количеством купленных продуктов (число взять из prompt()).  */

function goToShop() {
    return new Promise ((res, rej) => {
        let products = prompt('how many ingredients did you buy');
        isNaN(products) ? rej(products) : res(products);
    }); 
}
/* Создайте функцию makeDinner(), которая возвращает Promise с таймером. Через 3 секунды промис должен завершиться с текстом ‘Bon Appetit’. */

function makeDinner() {
    return new Promise ((res) => {
        setTimeout(() => {
            res(console.log('Bon Appetit'));
        }, 3000);
    });
}

/* Если функция goToShop() возвращает меньше чем 4 продукта, то вернуть отклоненный промис с текстом ‘Too low products’, иначе вызвать функцию makeDinner() и вывести в консоль результат работы функции.
Если промис возвращается с ошибкой, то вывести ошибку в консоль в console.error(); */

goToShop()
    .then( (value) => value < 4 ? console.error(new Error('Too low products')) : makeDinner())
    .catch(() => console.error(new Error('type a number')));
