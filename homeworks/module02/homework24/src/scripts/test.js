//Создать класс Animal со свойствами nickname, food, location и методом changeFood().
class Animal {
  constructor(nickname, food, location) {
    this.nickname = nickname;
    this.food = food;
    this.location = location;
  }
  changeFood(newFood) {
    this.food = newFood;
  }
}
//Создать классы Dog и Cat, которые наследуются от класса Animal и имеют дополнительные свойства (weight для класса Dog, isHomless (по-умолчанию true) для класса Cat).
class Dog extends Animal {
  constructor(nickname, food, location, weight) {
    super(nickname, food, location);
    this.weight = weight;
  }
}
class Cat extends Animal {
  constructor(nickname, food, location, isHomeless = true) {
    super(nickname, food, location);
    this.isHomeless = isHomeless;
  }
}
//Создать класс Person со свойствами firstName и lastName и методом getFullName().
class Person {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
  getFullName() {
    return this.firstName + " " + this.lastName;
  }
}
//Создать класс Hospital со свойством name, а также с приватными свойствами #illAnimals и #findingPetsPeople и методами:
/*  1)для получения данных из массивов: getAnimals() и getFindingPetsPeople();
    2)для добавления данных в массивы: addAnimal() и addPeople() (можно добавить сразу много людей);
    3)findHome() - проверяет находится ли животное в больнице, если да, возвращает объект: */
class Hospital {
  constructor(name) {
    this.name = name;
  }
  #illAnimals = [];
  #findingPetsPeople = [];
  getAnimals() {
    return this.#illAnimals;
  }
  getFindingPetsPeople() {
    return this.#findingPetsPeople;
  }
  addAnimal(object) {
    this.#illAnimals.push(object);
  }
  addPeople(person) {
    this.#findingPetsPeople = this.#findingPetsPeople.concat(person);
  }
  findHome(nickname) {
    let lostAnimal = this.#illAnimals.find(
      (value) => value.nickname === nickname
    ); // не работает?
    if (lostAnimal) {
      return {
        status: "restricted",
        message: `We need to heal ${nickname} firstly`,
      };
    } else {
      let randomIndex = Math.floor(
        Math.random() * this.#findingPetsPeople.length
      );
      let randomPerson = this.#findingPetsPeople.splice(randomIndex, 1);
      return {
        status: "success",
        name: `${randomPerson[0].getFullName()}`,
      };
    }
  }
  getAnimalsNicknames() {
    let animalsNicknames = [];
    this.#illAnimals.forEach((value) => animalsNicknames.push(value.nickname));
    return animalsNicknames;
  }
}
//Создать класс Veterinarian, который наследуется от класса Person и имеет новое свойство hospital и приватное (1) свойство #diagnosis = { ill = 'ill', healthy: 'healthy' }, а также следующие методы:
class Veterinarian extends Person {
  constructor(firstName, lastName, hospital) {
    super(firstName, lastName);
    this.hospital = hospital;
  }
  #diagnosis = {
    ill: ["overweight", "change food", "undefined ill"],
    healthy: "healthy",
  };
  //переопределенный метод getFullName(), который возвращает имя врача и название больницы, где он работает (например, Ivan Ivanov (Hospital));
  getFullName() {
    return this.firstName + " " + this.lastName + " " + `(${this.hospital})`;
  }
  //#setDiagnosis() (приватный метод), который возвращает объект диагноза пациенту
  #setDiagnosis(animal) {
    //Если у животного вес больше 20кг, то вернуть ill и 'overweight';
    if (animal.weight > 20) {
      return {
        diagnosis: `${this.#diagnosis.ill[0]}`,
        info: `I prescribe frequent jogging in the fresh air`,
      };
    }
    //Если животное ест корм, то изменить питание на 'meal with rice' и вернуть ill и 'change food. Now <nickname> eats <food>;
    if (animal.food === "cat food" || animal.food === "dog food") {
      animal.food = "meal with rice";
      return {
        diagnosis: `${this.#diagnosis.ill[1]}`,
        info: `Now ${animal.nickname} eats ${animal.food}`,
      };
    }
    //Если животное бездомное, то найти животному нового хозяина и если все хорошо, то вернуть healthy и 'change home. Now <firstName> <lastName> have a new friend - <nickname>', если нет - вернуть ill и сообщение, которое возвращает метод;
    if (animal.isHomeless) {
      let personWithNewFriend = this.hospital.findHome(animal.nickname);
      return {
        diagnosis: this.#diagnosis.healthy,
        info: `Now ${personWithNewFriend.name} have a new friend - ${animal.nickname}`,
      };
    } else {
      return {
        diagnosis: this.#diagnosis.ill[2],
        info: `some info message`,
      };
    }
  }
  //treatAnimal(), вызывает метод #setDiagnosis() - (если животное нужно лечить, добавляет его в больницу) и возвращает объект:
  treatAnimal(animal) {
    let diagnosis = this.#setDiagnosis(animal);
    if (diagnosis.diagnosis !== "healthy") {
      this.hospital.addAnimal(animal);
    }
    return {
      info: `${animal.nickname} from ${animal.location}`,
      fullDiagnos: `${diagnosis.diagnosis}: ${diagnosis.info}`,
    };
  }
}
/* Создайте функцию main(), в которой создайте больницу, врача, несколько разных типов животных и несколько людей, которые ищут себе животных.
Всех животных отправьте на прием к врачу и выведете в консоль информацию о процессе лечения. */

function main() {
  const hospital = new Hospital("Weight loss and shuffle animals clinic");
  const doctor = new Veterinarian("Tonny", "Round", hospital);
  const person1 = new Person("Ginger", "Lasta");
  const person2 = new Person("Sem", "Jem");
  const person3 = new Person("Jiji", "Kong");
  const person4 = new Person("Lorem", "Ipsum");
  const animal1 = new Cat("Rosa", "any", "central park", true);
  const animal2 = new Cat("Murka", "cat food", "park avenue 10/12", false);
  const animal3 = new Dog("Patron", "moskals tears", "top secret", 10);
  const animal4 = new Dog("Bobik", "porridge", "Petrovskaya 10, wooden cabin", 25);

  const persons = [person1, person2, person3, person4];
  const animals = [animal1, animal2, animal3, animal4];
  
  hospital.addPeople(persons);

  for (let animal of animals) {
    let animalStatus = doctor.treatAnimal(animal);
    console.group(doctor.getFullName());
    console.log(animalStatus.info);
    console.log(animalStatus.fullDiagnos);
    console.groupEnd();
  }
  //Выведите в консоль клички всех животных, которые лечатся в больнице.
  const illAnimalNicknames = hospital.getAnimalsNicknames();
  console.log("Animals in the hospital: " + illAnimalNicknames);
}
main();
