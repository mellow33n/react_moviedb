import { Hospital } from "./hospital";
import { Veterinarian } from "./veterinarian";
import { Person } from "./person";
import { Cat } from "./cat";
import { Dog } from "./dog";

/* Создайте функцию main(), в которой создайте больницу, врача, несколько разных типов животных и несколько людей, которые ищут себе животных.
Всех животных отправьте на прием к врачу и выведете в консоль информацию о процессе лечения. */

function main() {
    const hospital = new Hospital("Weight loss and shuffle animals clinic");
    const doctor = new Veterinarian("Tonny", "Round", hospital);
    const person1 = new Person("Ginger", "Lasta");
    const person2 = new Person("Sem", "Jem");
    const person3 = new Person("Jiji", "Kong");
    const person4 = new Person("Lorem", "Ipsum");
    const animal1 = new Cat("Rosa", "any", "central park", true);
    const animal2 = new Cat("Murka", "cat food", "park avenue 10/12", false);
    const animal3 = new Dog("Patron", "moskals tears", "top secret", 10);
    const animal4 = new Dog("Bobik", "porridge", "Petrovskaya 10, wooden cabin", 25);
  
    const persons = [person1, person2, person3, person4];
    const animals = [animal1, animal2, animal3, animal4];
    
    hospital.addPeople(persons);
  
    for (let animal of animals) {
      let animalStatus = doctor.treatAnimal(animal);
      console.group(doctor.getFullName());
      console.log(animalStatus.info);
      console.log(animalStatus.fullDiagnos);
      console.groupEnd();
    }
    //Выведите в консоль клички всех животных, которые лечатся в больнице.
    const illAnimalNicknames = hospital.getAnimalsNicknames();
    console.log("Animals in the hospital: " + illAnimalNicknames);
  }
  main();