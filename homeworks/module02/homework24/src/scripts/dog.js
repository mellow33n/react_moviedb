import { Animal } from "./animal";
//Создать классы Dog и Cat, которые наследуются от класса Animal и имеют дополнительные свойства (weight для класса Dog, isHomless (по-умолчанию true) для класса Cat).
export class Dog extends Animal {
    constructor(nickname, food, location, weight) {
      super(nickname, food, location);
      this.weight = weight;
    }
}