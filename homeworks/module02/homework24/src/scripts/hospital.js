//Создать класс Hospital со свойством name, а также с приватными свойствами #illAnimals и #findingPetsPeople и методами:
/*  1)для получения данных из массивов: getAnimals() и getFindingPetsPeople();
    2)для добавления данных в массивы: addAnimal() и addPeople() (можно добавить сразу много людей);
    3)findHome() - проверяет находится ли животное в больнице, если да, возвращает объект: */
export class Hospital {
    constructor(name) {
      this.name = name;
    }
    #illAnimals = [];
    #findingPetsPeople = [];
    getAnimals() {
      return this.#illAnimals;
    }
    getFindingPetsPeople() {
      return this.#findingPetsPeople;
    }
    addAnimal(object) {
      this.#illAnimals.push(object);
    }
    addPeople(person) {
      this.#findingPetsPeople = this.#findingPetsPeople.concat(person);
    }
    findHome(nickname) {
      let lostAnimal = this.#illAnimals.find(
        (value) => value.nickname === nickname
      ); // не работает?
      if (lostAnimal) {
        return {
          status: "restricted",
          message: `We need to heal ${nickname} firstly`,
        };
      } else {
        let randomIndex = Math.floor(
          Math.random() * this.#findingPetsPeople.length
        );
        let randomPerson = this.#findingPetsPeople.splice(randomIndex, 1);
        return {
          status: "success",
          name: `${randomPerson[0].getFullName()}`,
        };
      }
    }
    getAnimalsNicknames() {
      let animalsNicknames = [];
      this.#illAnimals.forEach((value) => animalsNicknames.push(value.nickname));
      return animalsNicknames;
    }
  }