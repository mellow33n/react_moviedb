//Создать класс Person со свойствами firstName и lastName и методом getFullName().
export class Person {
    constructor(firstName, lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
    }
    getFullName() {
      return this.firstName + " " + this.lastName;
    }
}