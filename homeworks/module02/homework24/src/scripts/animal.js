//Создать класс Animal со свойствами nickname, food, location и методом changeFood().
export class Animal {
    constructor(nickname, food, location) {
      this.nickname = nickname;
      this.food = food;
      this.location = location;
    }
    changeFood(newFood) {
      this.food = newFood;
    }
  }
