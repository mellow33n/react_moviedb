import { Animal } from "./animal";
//Создать классы Dog и Cat, которые наследуются от класса Animal и имеют дополнительные свойства (weight для класса Dog, isHomless (по-умолчанию true) для класса Cat).
export class Cat extends Animal {
    constructor(nickname, food, location, isHomeless = true) {
      super(nickname, food, location);
      this.isHomeless = isHomeless;
    }
}