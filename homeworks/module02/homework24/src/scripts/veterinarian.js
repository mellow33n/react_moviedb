import { Person } from "./person";

//Создать класс Veterinarian, который наследуется от класса Person и имеет новое свойство hospital и приватное (1) свойство #diagnosis = { ill = 'ill', healthy: 'healthy' }, а также следующие методы:
export class Veterinarian extends Person {
    constructor(firstName, lastName, hospital) {
      super(firstName, lastName);
      this.hospital = hospital;
    }
    #diagnosis = {
      ill: ["overweight", "change food", "undefined ill"],
      healthy: "healthy",
    };
    //переопределенный метод getFullName(), который возвращает имя врача и название больницы, где он работает (например, Ivan Ivanov (Hospital));
    getFullName() {
      return this.firstName + " " + this.lastName + " " + `(${this.hospital})`;
    }
    //#setDiagnosis() (приватный метод), который возвращает объект диагноза пациенту
    #setDiagnosis(animal) {
      //Если у животного вес больше 20кг, то вернуть ill и 'overweight';
      if (animal.weight > 20) {
        return {
          diagnosis: `${this.#diagnosis.ill[0]}`,
          info: `I prescribe frequent jogging in the fresh air`,
        };
      }
      //Если животное ест корм, то изменить питание на 'meal with rice' и вернуть ill и 'change food. Now <nickname> eats <food>;
      if (animal.food === "cat food" || animal.food === "dog food") {
        animal.food = "meal with rice";
        return {
          diagnosis: `${this.#diagnosis.ill[1]}`,
          info: `Now ${animal.nickname} eats ${animal.food}`,
        };
      }
      //Если животное бездомное, то найти животному нового хозяина и если все хорошо, то вернуть healthy и 'change home. Now <firstName> <lastName> have a new friend - <nickname>', если нет - вернуть ill и сообщение, которое возвращает метод;
      if (animal.isHomeless) {
        let personWithNewFriend = this.hospital.findHome(animal.nickname);
        return {
          diagnosis: this.#diagnosis.healthy,
          info: `Now ${personWithNewFriend.name} have a new friend - ${animal.nickname}`,
        };
      } else {
        return {
          diagnosis: this.#diagnosis.ill[2],
          info: `some info message`,
        };
      }
    }
    //treatAnimal(), вызывает метод #setDiagnosis() - (если животное нужно лечить, добавляет его в больницу) и возвращает объект:
    treatAnimal(animal) {
      let diagnosis = this.#setDiagnosis(animal);
      if (diagnosis.diagnosis !== "healthy") {
        this.hospital.addAnimal(animal);
      }
      return {
        info: `${animal.nickname} from ${animal.location}`,
        fullDiagnos: `${diagnosis.diagnosis}: ${diagnosis.info}`,
      };
    }
  }