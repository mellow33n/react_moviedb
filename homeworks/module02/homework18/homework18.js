
/* Создайте класс Worker со следующими свойствами name, surname, rate, days. Напишите внутри класса метод getSalary(), который считает зарплату (рейт умноженный на количество отработанных дней) и метод getInfo(), который возвращает строку с информацией о полученой зарплате сотрудника */
class Worker {
    constructor(name, surname, rate, days) {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
    }
    get salary() {
        return (this.rate * this.days).toFixed(2);
    }
    get info() {
        return console.log(`${this.name} ${this.surname} got ${this.salary}`);
    }
}

/* Создайте класс Boss, который наследуется от класса Worker. Этот класс имеет те же свойства, что и Worker и плюс дополнительное свойство totalProfit. Напишите метод getSalary(), который считает зарплату сотрудника так же как метод getSalary() класса Worker + 10% от прибыли (totalProfit)

Чтобы посчитать переменную totalProfit - нужно найти сумму зарплат всех сотрудников на позиции worker. Можете найти эту переменную отдельно вне классов с помощью методов массива и передавать в класс Boss уже константой. */

const totalProfit = empoyees.reduce((accamulate, value) => value.position === 'worker' ? accamulate + value.days * value.rate : accamulate + 0, 0);

class Boss extends Worker {
    constructor(name, surname, rate, days) {
        super(name, surname, rate, days);
    }
    get salary() {
        return (this.rate * this.days + totalProfit * 0.1).toFixed(2);
    }
}
/* Создайте класс Trainee, который наследуется от класса Worker. Этот класс имеет те же свойства, что и Worker, но его метод geSalary() работает следующим образом. Во время испытательного срока (до 60 дней) сотрудник получает 70% своей зарплаты, а после испытательного срока так же как обычный сотудник. */

class Trainee extends Worker {
    constructor(name, surname, rate, days) {
        super(name, surname, rate, days);
    }
    get salary() {
        const traineeDays = 60;
        const traineeRate = this.rate * 0.7;
        if (this.rate > traineeDays) {
            const richDays = this.rate - traineeDays;
            return (traineeDays * traineeRate + richDays * this.rate).toFixed(2);
        }
        return (this.days * traineeRate).toFixed(2);
    }
}

/* Возьмите массив объектов представленный в файле ниже (employee.js) и создайте для каждого объекта этого массива новый объект соответсвующего класса (какой класс выбрать укажет свойсвто position).
Продемонстрируйте метод getInfo() для каждого объекта в массиве. */
for (let emp of empoyees) {
    switch (emp.position) {
        case 'worker':
            emp.class = new Worker(emp.name, emp.surname, emp.rate, emp.days);
            emp.class.info;
            break;
        case 'boss':
            emp.class = new Boss(emp.name, emp.surname, emp.rate, emp.days);
            emp.class.info;
            break;
        case 'trainee':
            emp.class = new Trainee(emp.name, emp.surname, emp.rate, emp.days);
            emp.class.info;
            break;
    }
}
/* Напишите задачу двумя способами (!): через классы и функцию-конструктор. */
function WorkerFn(name, surname, rate, days) {
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
}
WorkerFn.prototype.getSalary = function () {
    return (this.rate * this.days).toFixed(2);
};
WorkerFn.prototype.getInfo = function () {
    return console.log(`${this.name} ${this.surname} got ${this.getSalary()}`);
};

function BossFn(name, surname, rate, days) {
    WorkerFn.call(this);
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
}
BossFn.prototype.getSalary = function () {
    return (this.rate * this.days + totalProfit * 0.1).toFixed(2);
};
BossFn.prototype.getInfo = function () {
    return console.log(`${this.name} ${this.surname} got ${this.getSalary()}`);
};
function TraineeFn(name, surname, rate, days) {
    WorkerFn.call(this);
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
}
TraineeFn.prototype.getSalary = function () {
    const traineeDays = 60;
    const traineeRate = this.rate * 0.7;
    if (this.rate > traineeDays) {
        const richDays = this.rate - traineeDays;
        return (traineeDays * traineeRate + richDays * this.rate).toFixed(2);
    }
    return (this.days * traineeRate).toFixed(2);
};
TraineeFn.prototype.getInfo = function () {
    return console.log(`${this.name} ${this.surname} got ${this.getSalary()}`);
};

for (let emp of empoyees) {
    switch (emp.position) {
        case 'worker':
            emp.class = new WorkerFn(emp.name, emp.surname, emp.rate, emp.days);
            emp.class.getInfo();
            break;
        case 'boss':
            emp.class = new BossFn(emp.name, emp.surname, emp.rate, emp.days);
            emp.class.getInfo();
            break;
        case 'trainee':
            emp.class = new TraineeFn(emp.name, emp.surname, emp.rate, emp.days);
            emp.class.getInfo();
            break;
    }
}
