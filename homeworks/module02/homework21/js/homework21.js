/* 1) Напишите функцию, которая получает с сервера массив персонажей мультфильма и отрисовывает их на странице в карточках.
Данные в карточках заполнять согласно тестовому примеру в html файле.
Если персонаж мертв, нужно добавлять класс dead к диву с классом 'live-status'. 
По-умолчанию, загружаем 10 случайных персонажей (создать функцию для получения массива 10ти случайных чисел, а потом передать их в URL );*/

const urlCharacters = "https://rickandmortyapi.com/api/character/";
function randomInteger(min, max) {
  return Math.floor(min + Math.random() * (max + 1 - min));
}

function getCharacters(url) {
  //беру даннные из сервера
  let charactersData = fetch(`${url}`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      if (url === "https://rickandmortyapi.com/api/character/") {
        //определяю 10 рандомных персонажей
        charactersData = Array.from(
          new Array(10),
          (value) => (value = randomInteger(1, data.info.count))
        ).toString();
        return fetch(`${url + charactersData}`).then((response) => {
          return response.json();
        });
      } else {
        // строю вилку цепей промисов для обычной жизни и функции фильтров
        return data.results;
      }
    })
    .then((charactersData) => {
      for (let character of charactersData) {
        createCard(character);
      }
    });
}
getCharacters(urlCharacters);

//2) Напишите функцию, которая по клику на фильтр вверху фильтрует персонажей по соответствующим критериям (male/female) или (alive/dead). Учтите, что фильтрация может быть как по одному признаку так и по обоим одновременно.

document.addEventListener("click", function (event) {
  let urlFilter = urlCharacters;
  let cards = document.querySelectorAll(".card");
  const male = document.querySelector("#male");
  const female = document.querySelector("#female");
  const alive = document.querySelector("#alive");
  const dead = document.querySelector("#dead");

  console.dir(event.target);
  //Условие, когда первая кнопка на male
  if (male.checked) {
    switch (event.target.id) {
      case "female":
        urlFilter = urlCharacters + "?gender=female";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
    }
    
    switch (event.target.id) {
      case "alive":
        urlFilter = urlCharacters + "?status=alive" + "&" + "gender=male";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
      case "dead":
        urlFilter = urlCharacters + "?status=dead" + "&" + "gender=male";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
    }
  }
  //Условие, когда первая кнопка на female
  if (female.checked) {
    switch (event.target.id) {
      case "male":
        urlFilter = urlCharacters + "?gender=male";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
    }
    switch (event.target.id) {
      case "alive":
        urlFilter = urlCharacters + "?status=alive" + "&" + "gender=female";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
      case "dead":
        urlFilter = urlCharacters + "?status=dead" + "&" + "gender=female";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
    }
  }
  //Условие, когда первая кнопка на Alive
  if (alive.checked) {
    switch (event.target.id) {
      case "male":
        urlFilter = urlCharacters + "?gender=male" + "&" + "status=alive";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
      case "female":
        urlFilter = urlCharacters + "?gender=female" + "&" + "status=alive";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
    }
    switch (event.target.id) {
      case "dead":
        urlFilter = urlCharacters + "?status=dead";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
    }
  }
  //Условие, когда первая кнопка на Dead
  if (dead.checked) {
    switch (event.target.id) {
      case "male":
        urlFilter = urlCharacters + "?gender=male" + "&" + "status=alive";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
      case "female":
        urlFilter = urlCharacters + "?gender=female" + "&" + "status=alive";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
    }
    switch (event.target.id) {
      case "alive":
        urlFilter = urlCharacters + "?status=alive";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
    }
  }

  //Обработка данных для первого клика по радиокнопкам
  if (!male.checked || !female.checked || !alive.checked || !dead.checked) {
    switch (event.target.id) {
      case "male":
        urlFilter = urlCharacters + "?gender=male";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
      case "female":
        urlFilter = urlCharacters + "?gender=female";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
      case "alive":
        urlFilter = urlCharacters + "?status=alive";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
      case "dead":
        urlFilter = urlCharacters + "?status=dead";
        for (let card of cards) {
          card.remove();
        }
        getCharacters(urlFilter);
        break;
    }
  }
});
