/* Задача:
Напишіть програму, яка питає у користувача дату в форматі YYYY-MM-DD і виводить в консоль всі дати від заданої дати до сьогоднішньої.

Обовʼязкові критерії:
дата повинна бути валідна і в строго заданому форматі YYYY-MM-DD (краще за все перевіряється за допомогою регулярних виразів);
формат виводу даних довільний, але обовʼязково відображати день; */
let initialDate = prompt('type data in YYYY-MM-DD format', '2022-06-30');
while(!isDateValid(initialDate)) {
    initialDate = prompt('type data in YYYY-MM-DD format', '2022-06-30');
}

 function isDateValid(dateStr) {
    const regex = /^\d{4}-\d{2}-\d{2}$/;
    if (dateStr.match(regex) === null) {
      return false;
    }
    const date = new Date(dateStr);
    const timestamp = date.getTime();
    if (typeof timestamp !== 'number' || Number.isNaN(timestamp)) {
      return false;
    }
    return date.toISOString().startsWith(dateStr);
}

initialDate = Date.parse(new Date(initialDate));

 const date = {
    finish: +initialDate, 
    start: Date.parse(new Date()),
    [Symbol.iterator]() {
        //вычесляю разницу в днях и окруляю до целого
        const dayMilliseconds = 24*60*60*1000;
        let delta = Math.floor(((this.start - this.finish) / dayMilliseconds));
        let stepDay = this.finish;
        return {
            today: this.start,
            userNumber: this.finish,
            next() {
                if (delta > 0) {
                    --delta;
                    stepDay += dayMilliseconds;
                    return { done: false, value: stepDay}
                }
                if (delta < 0) {
                    ++delta;
                    stepDay -= dayMilliseconds;
                    return { done: false, value: stepDay}
                }
                return {done: true, value: stepDay}
            },
        };
    },
};
function parseDate(date) {
    const offset = date.getTimezoneOffset()
    date = new Date(date.getTime() - (offset*60*1000))
    return date.toISOString().split('T')[0]
}

for (let value of date) {
  console.warn('Date: ', parseDate(new Date(value)) ); // <- виводимо дані в консоль в довільному форматі
}