"use strict";
const mainURL = 'http://api.weatherstack.com/current';
const accessKey = localStorage.accessKey;
const pickTowns = ['Kiev', 'Lviv', 'Kharkiv', 'Dnepropetrovsk', 'Odesa'];

//заполняю города при загрузке
document.addEventListener('DOMContentLoaded', () => {
    const liElems = document.querySelectorAll('li');
    let i = 0;
    liElems.forEach((value) => {
        value.textContent = pickTowns[i];
        i++
    });
    getWeather(pickTowns[0]);
});

//функция для отрисовки карточки
function loadItem (city, status, imgURL, temp, feelsLikeTemp) {
    
    //создаю необходимые блоки
    let divWeatherHeader = document.createElement('div');
    divWeatherHeader.setAttribute('class', 'weather__header');

    let divWeatherMain = document.createElement('div');
    divWeatherMain.setAttribute('class', 'weather__main');

    let divWeatherCity = document.createElement('div');
    divWeatherCity.setAttribute('class', 'weather__city');
    divWeatherCity.textContent = `${city}`;

    let divWeatherStatus = document.createElement('div');
    divWeatherStatus.setAttribute('class', 'weather__status');
    divWeatherStatus.textContent = `${status}`;

    let divWeatherIcon = document.createElement('div');
    divWeatherIcon.setAttribute('class', 'weather__icon');

    let imgTag = document.createElement('img');
    imgTag.setAttribute('src', `${imgURL}`);
    imgTag.setAttribute('alt', 'weather-image');

    let divWeatherTemp = document.createElement('div');
    divWeatherTemp.setAttribute('class', 'weather__temp');
    divWeatherTemp.textContent = `${temp}`;

    let divWeatherfeelsLike = document.createElement('div');
    divWeatherfeelsLike.setAttribute('class', 'weather__feels-like');
    divWeatherfeelsLike.textContent = `${feelsLikeTemp}`;
    //удаляю лоадер
    let divWeather = document.querySelector('.weather');
    sarahLovesCleanless();

    //формирую карточку
    divWeather.append(divWeatherHeader);
    divWeather.append(divWeatherTemp);
    divWeather.append(divWeatherfeelsLike);

    let divWeatherHeaderSelector = document.querySelector('.weather__header');
    divWeatherHeaderSelector.append(divWeatherMain);
    divWeatherHeaderSelector.append(divWeatherIcon);

    let divWeatherMainSelector = document.querySelector('.weather__main');
    divWeatherMainSelector.append(divWeatherCity);
    divWeatherMainSelector.append(divWeatherStatus);

    let divWeatherIconSelector = document.querySelector('.weather__icon');
    divWeatherIconSelector.append(imgTag);
}

//функция для ожидания
function pendingItem() {
    let divWeather = document.querySelector('.weather');
    sarahLovesCleanless();
    
    let divWeatherLoading = document.createElement('div');
    divWeatherLoading.setAttribute('class', 'weather__loading');

    let imgTagLoading = document.createElement('img');
    imgTagLoading.setAttribute('src', './img/loading.gif');
    imgTagLoading.setAttribute('alt', 'loading-gif');

    divWeather.append(divWeatherLoading);
    let divWeatherLoadingSelector = document.querySelector('.weather__loading');
    divWeatherLoadingSelector.append(imgTagLoading);

}
//функция для зачистки карточки
function sarahLovesCleanless () {
    let divWeather = document.querySelector('.weather');
    //это не ошибка, по неведомой мне причине с первого раза не зачищает карточку полностью
    divWeather.childNodes.forEach((value) => value.remove());
    divWeather.childNodes.forEach((value) => value.remove());
    divWeather.childNodes.forEach((value) => value.remove());
}

//внимание, а теперь красота:
async function getWeather(city) {
    pendingItem();
    try {
        const request = await fetch(`${mainURL}?access_key=${accessKey}&query=${city}`);
        const response = await request.json();
        return loadItem(
            response.location.name, 
            response.current.weather_descriptions[0], 
            response.current.weather_icons[0], 
            response.current.temperature, 
            response.current.feelslike
            );
    } catch(error) {
        return error
    }
}

//добавляю логику для кликов
document.addEventListener('click', (event) => {
    if(event.target.className === 'animate') {
        const liElems = document.querySelectorAll('li');
        for (let li of liElems) {
            li.className = 'animate';
        };
        event.target.classList.add('selected');

        getWeather(event.target.innerText)
    }
})



